
Since the Singapore Prestige Brand Award was established in 2002, the Award has grown to become a mark of success that motivates other local brands to distinguish themselves through initiating new and effective branding initiatives. Past award winners, such as Yeo’s and Eu Yan Sang, have become established names in the region and the world.

Innovation as part of a Trusted Brand

Branding is increasingly important today, especially in a time when it is difficult to retain the attention of our consumers. Building a strong brand that people know and relate to will continue to be essential to the success of any company. However, becoming a trusted name requires something more. 

A trusted brand could be one that provides quality products, good service or is renowned for caring for its employees. A trusted brand could also be one that has proven its ability to innovate and evolve to remain competitive and relevant in a fast changing world.

In this day and age, embracing digital transformation has become a key part of building a successful and trusted brand. It allows a company to not only achieve better operational efficiency but also understand and reach out to a larger customer base from around the world.

Supporting our SMEs in Building Strong Digital Capabilities

While this is important for both big and small companies, we know that SMEs face some specific challenges in adopting digital technologies in their growth journey.

As such, I announced earlier this year the “SMEs Go Digital” programme, to provide more structured support for SMEs that wish to use digital technologies to boost their productivity, or to enhance their digital capabilities in areas such as cybersecurity, data protection and data analytics.

Under this programme, SMEs can access proven digital technology solutions ranging from digital ordering and payment, to supply chain optimization. To be listed on the Tech Depot, the solutions would have undergone stringent evaluation by IMDA before being pre-approved for SMEs’ use. This gives SMEs peace of mind in terms of the solutions’ capability and vendors’ reliability.

Recently, I visited SMEs from the Security and Cleaning sectors. It was encouraging to see that some of these pre-approved solutions have helped SMEs become more productive while reducing their costs.

One such company is Royal Security, a security services company that started its business 30 years ago.  The SME has a vast experience in providing security services to commercial, industrial and residential buildings. As part of their efforts to adopt digital technologies into their operations, they deployed the SMEs Go Digital pre-approved solution, iREP Security System. 

Since deploying the solution, Royal Security can now monitor the performance of the security team in real time. Security Officers can also communicate more effectively with the Operations Manager back in office as incident reports can be generated on the spot, using a single device to capture images, time and place. With more accurate information on hand, the Operations Manager can now make timely rectification. This has led to time savings of 8 to 10 hours per week as operations staff no longer need to travel to individual sites to verify incidents. 

In addition to the enhancement in the company’s operational efficiency, this has further reduced payroll errors and disputes by up to 80%.

The system has also proven to be able to scale up to cover more buildings, with Royal Security expanding the project from an initial 16% to 70% of their total sites. Royal Security has also indicated that they would like to adopt the solution for all of their sites in future. 

I believe we can all learn from Royal Security’s example and we look forward to helping more SMEs tap on such pre-approved solutions. To this end, I am happy to share that IMDA will be expanding the current list of pre-approved solutions and putting up a call for the pre-approval of solutions in the coming months. I invite all interested solution providers to come forward to participate in this standing call. More details will be shared at a later date. 

Advising SMEs on more advanced digital needs 

We also recognise that the digital needs of SMEs vary widely across and within sectors. We acknowledge some SMEs may need assistance with selecting the right technologies for their business. 

Hence, the Government has established a network of SME Centres as the first touch points for SMEs. The SME Centres offer easily accessible business advisory to help SMEs further develop their business. These include business diagnosis, advisory on Government schemes as well as capability workshops. SMEs can also seek free one-to-one consultation sessions with business advisors to select digital technology solutions that meet their business needs.

Our trade associations and chambers (TACs) are also well-placed to strengthen the capabilities of SMEs as they best understand the sector’s needs. To this end, IMDA will continue to work with TACs, like the Association of Small and Medium Enterprises (ASME) to engage SMEs in their digitalisation journey. The SME Digital Tech Hub, which opened its door in end September, is operated by ASME and will provide in-person digital technology consultancy for SMEs with more advanced digital needs.

Besides providing advisory, the SME Digital Tech Hub will also help to connect SMEs to suitable ICT vendors and consultants, as well as conduct workshops and seminars to help SMEs to build their digital capabilities.

I am pleased to hear that since it became operational, 4 companies have stepped forward to work with the SME Digital Tech Hub to take full advantage of digital technology to change their business processes.

One of the companies engaging the SME Digital Tech Hub is Hock Seng Hoe Metal Company, one of the winners of this year’s Singapore Prestige Brand Award. The company was established in 2009 and is now a reputable steel processing provider.

With the aim of providing their clients with a timely, cost-effective and professional service, the company engaged the SME Digital Tech Hub to develop a digitalisation roadmap that looks at improving their entire end-to-end business processes from raw material management to sales, delivery service and backend operations like Finance and Human Resources. Beyond that, they also hope it would enable the management to make more informed decisions.

Another SME that has engaged the SME Digital Tech Hub is homegrown travel services company Transtar Travel. Since its inception in 1994, Transtar has become one of the leading express coach companies in Singapore, plying routes from Singapore to major cities in Malaysia. 

However, with the rapid rise of online travel portals, Transtar Travel saw the need to digitalise to reach out to customers across the region as well as provide a better user experience. The need to go digital was further exacerbated company’s manual processes for customer interaction and transactions, coach scheduling and fleet management that do not allow for integration. 

Transtar Travel is currently working with the SME Digital Tech Hub to develop a digitalisation roadmap and implement end-to-end digital solutions for its entire business processes and they hope to improve cost effectiveness, achieve savings from manpower cost and provide a better all-round experience for their customers.

Royal Security, Hock Seng Hoe Metal Company and Transtar Travel are just three examples of SMEs that have embraced technology to transform their business and brand to thrive in the digital economy. I look forward to seeing more of our SMEs take the step up to adopt technology in their businesses.