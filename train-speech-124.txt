In the Digital Economy, organisations from all sectors will need to be agile and harness infocomm technologies to boost their capabilities and to stay competitive.  At the Committee of Supply Debates last week, my Ministry outlined plans to help our workforce and businesses ready themselves for our digital economy – by adopting the “ABC” formula. “A” to accelerate the digitalisation of existing sectors, “B” to build up our info-comm media companies, and “C” to create Singapore’s future technology sectors.

Grooming digital leaders

IMDA’s partnership with Google and AVADO1 to introduce the Squared Online for SMEs Programme is an example of Strategy A in action. The Squared Online for SMEs programme provides SMEs with a strategic understanding of today’s changing consumer landscape, and let their decision makers and leaders develop the skills to drive digital transformation for their companies. 

I would also like to commend the companies present today for investing in your employees and developing them to be future digital leaders.  Close to 100 business leaders from nearly 80 companies in Singapore have completed the first run of the Squared Online for SMEs programme.

One of the companies is Mothercare. Mr Rain Khoo, General Manager of e-commerce and Mr Michael Tan, Digital Marketing Executive, put their skills to the test by leveraging new platforms and technologies to reach out to a wider audience. Catching on the trend of video content, Michael put together a product video content which attracted over 300,000 views and close to 200 comments for a recent Mothercare event.

In the case of Mr Alvin Ong, Executive Director, Hiap Tong Corporation – which is in the construction industry – he took part in the programme to understand how technology can help them establish their branding and grow new markets. Hiap Tong Corporation will be leveraging digital trends, platforms and methods, such as paid searches and keyword ranking, to strengthen their presence in both local and global markets.

Upcoming plans

The Government will continue to support our workers and companies to develop talent and capabilities so that they can seize good opportunities in the Digital Economy.

We will scale up the Tech Skills Accelerator, or TeSA for short with an additional $145m of funding and provide an additional 20,000 training places by 2020. Through the TeSA initiative, we will work with suitable partners to scale the leadership programmes to cultivate digital strategists and leaders.

An example would be IMDA’s recent collaboration with the Human Capital Leadership Institute (HCLI) to offer an Advanced Digital Leadership Programme (ADLP) to help organisations equip leaders with deep understanding of technology and digital business strategies so that they can lead digital transformation in their organisations. I look forward to working with Google and other industry partners to develop and equip local leaders with digital leadership capabilities to enable organisations to transform and remain globally competitive.