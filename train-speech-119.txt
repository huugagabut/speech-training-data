Digital Readiness in an Inclusive Digital Society

Building an inclusive digital society requires a many-hands approach. Everyone has a role to play in this meaningful journey. And it has been heart-warming to see the community come together – with people from different backgrounds, ages, races – all stepping forward to help seniors embrace technology in their daily lives.

I would like to first commend our Silver Infocomm Wellness Ambassadors (SIWA). I had the pleasure of meeting some of you at previous Silver Infocomm events.  You are the savvy seniors who have become our advocates in the community. You are indeed role models of what active ageing is about, learning about technology and sharing your knowledge through teaching and guiding other seniors. One of our Silver Infocomm Wellness Ambassadors is Mdm Cheow Chin Wang. Mdm Cheow’s first experience with a computer was just ten years ago and she embraced this new experience. Today, 67-year-old Mdm Cheow is a one of RSVP Singapore’s most popular trainers, teaching her peers how to use Windows and even coding! 

However, it is not just the case of seniors who are showing other seniors the joy of using technology. I would like to share the story of Syafiq Lee Bin Sani Lee. Syafiq is a second year student at ITE Central College. Last year, he joined his friends to volunteer at our Silver IT Fest as part of his Co-Curricular Activities as a member of the Media Smart Club. At the event, Syafiq helped many seniors learn how to use social media and cloud storage services. He found his one-day stint such a fulfilling one that he has signed up to volunteer for all three days at this year’s Silver IT Fest. I heard that he is now the nominated President of the Media Smart Club and is aiming to rally more of his peers to join him as a volunteer at the event.  

Another volunteer I would like to highlight is Mr Suresh Pathak, who came to Singapore from India three years ago and is now working at IBM Singapore. Despite having to travel overseas for work on a regular basis, he still manages to find time in his hectic schedule to volunteer at the Digital Clinics. There, he helps to address technology-related queries from our seniors and shows them how to tap on useful features in their mobile devices. Syafiq, Mdm Cheow, and Suresh are all here with us tonight. I think we should give them and all our other volunteers, a big round of applause.  

For our people to seize the opportunities and enjoy the benefits that digital technology brings, every citizen needs to be digitally ready, in their workplace, at school and as they go about with their daily lives. Our seniors too must not be left out. Through your work and efforts, we have been able to enable and empower 200,000 seniors over the years. Please keep up your good work in making sure we can build a digitally inclusive society.

Expanded Digital TV Assistance Scheme

Some of you were able to enjoy the Mediacorp Experience programme this afternoon, and learn how Singapore’s broadcasting industry has changed over the years. At the end of this year, 2018, Singapore TV will be making another big shift – to Digital TV. All Mediacorp analogue TV channels will be switched off after 31 December 2018. Viewers will have to switch to Digital TV, or DTV, to continue watching their favourite Mediacorp TV programmes.

Since September 2014, IMDA has implemented the DTV Assistance Scheme to help our low-income households switch over to DTV. I am pleased to announce tonight that IMDA will be expanding this Scheme, by offering a DTV Starter Kit to all Singaporean HDB households who are not subscribed to a Pay TV service. 

With the DTV Starter Kit, these households can have the option of having the DTV equipment delivered to them and installed all for free. Alternatively, they can have the option of offsetting the purchase of DTV equipment at participating electronics stores. Through the DTV Starter Kit, the Government is committed to help Singaporeans transition to DTV and enjoy its benefits. Eligible households will receive letters explaining how to redeem their DTV Starter Kit in the coming months. Those staying in Clementi and Tampines will be the first batch of households receiving the letters. 

IMDA will also raise awareness and step up assistance in the community to help Singaporeans switch to DTV. Residents can look forward to regular roadshows in major housing estates throughout the year, where they will experience the benefits of DTV and receive help to redeem their DTV Starter Kit.

In the same way that we help our seniors learn and enjoy using technology, we want to ensure that as many households as possible can enjoy DTV as possible. I would like to encourage everyone here to help your family, friends and neighbours migrate to DTV. 

Conclusion

By giving, one often receives, too. By helping Singaporeans to be digitally ready, we also strengthen our inter-generational bonds, build greater social cohesion and understanding within the community.

Our Government is committed to work with all of you in this effort.  I would like to thank all of you once again for your contributions and I look forward to what we can achieve together. Thank you. 

