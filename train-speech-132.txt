Seizing Opportunities in the Digital Economy

	Good morning. Thank you for joining me to launch the Skills Framework for Infocomm Technology today.

The Infocomm Media or ICM sector is an exciting, growth sector with many job and business opportunities for both Singaporeans and Singapore companies. Last week, we launched the ICM Industry Transformation Map, which detailed the plans to transform the ICM sector for our future economy. 

As part of the Industry Transformation Map, we have set out the manpower and skills development strategies for the ICT sector. This is a key area of focus as people are the key driver of growth and capability in the technology sector. We want to develop talent with the relevant skills to power the digital transformation of our economic sectors. 

The demand for ICT professionals in Singapore is set to grow. We project a strong demand for more than 42,ICT professionals over the next years. Hence, we also want to ensure that Singaporeans are equipped with the relevant skills to benefit from these good jobs.

Our Government is committed to prepare individuals and companies for the opportunities in the digital economy. We have introduced the TechSkills Accelerator initiative or TeSA to support individuals and companies to develop a skilled ICT talent pipeline. Till date, TeSA has enabled more than 16,ICT professionals to up-skill and re-skill themselves.

As part of this effort, we also set out to refresh the National Infocomm Competency Framework to keep pace with the fast evolving technology landscape. Hence, we have developed the Skills Framework for ICT, which we are launching today.

What is the Skills Framework for ICT? Why is it Essential?

The Skills Framework for ICT provides career information on the ICT sector, including career pathways, job roles, skills, competencies and training programmes. It spells out the core workplace competencies for ICT professionals across all economic sectors. It also identifies existing and emerging technical skills and competencies, and provides information on more than ICT job roles. 

It is therefore a useful repository of information that individuals, employers and training providers can refer to, to guide the training, career planning and human capital development of ICT professionals in the digital economy. 

The Skills Framework for ICT is not only relevant for the ICT sector but it can be used across other sectors. This means that employers or individuals in non-ICT sectors, such as retail, logistics and finance, can also use the Framework. It was designed to focus on the types of skills and competencies required for different ICT job roles, and is not biased towards specific technology tools. This ensures that the Skills Framework is relevant to ICT professionals across different industries.

Who Benefits from It?

The Skills Framework for ICT is designed to benefit employers and individuals who are either already in an ICT job role or planning to join the ICT profession. Individuals, employers and training providers can refer to the Skills Framework for ICT for various purposes. 

Employers

For employers, it will enable them to make better decisions about where to invest their training dollars and strategise their talent recruitment, development and management. So far, more than ten organisations have come on board as early adopters of the Skills Framework for ICT to benefit specific human resource areas in their organisations.  Let me share a few examples.

To support its transformation into a next generation multimedia and ICT services company, Singtel will be using the Framework to develop job profiles to attract new engineering and cybersecurity talents with the relevant skills, as well as to develop training roadmaps to up-skill existing workers.

Similarly, the Integrated Health Information Systems or IHiS will be leveraging the Framework to identify and address skills gap and training needs in the organisation. This is to ensure that its workers can stay relevant amidst the disruption and transformation that the healthcare industry is going through.

The Framework is also useful for SMEs. i-Sprint Innovations, a local SME, will be using the Framework to guide and improve their recruitment process through clear identification of the skills required for various job roles.

Organisations seeking to adopt the Skills Framework for ICT as part of their workplace transformation can also consider leveraging available tools, such as the Singapore National Employers Federation’s SAPPHIRE initiative. Under this initiative, companies can receive subsidy from Workforce Singapore, which will cover up to hours of consultancy advice.

Individuals

Apart from employers, the Skills Framework for ICT will definitely benefits individuals. For students or existing workers who wish to join or progress within their ICT careers, they can use the Framework to assess their career interest, identify relevant training programmes to upgrade their skills, and prepare for their desired jobs. Let me share with you two interesting individuals who joined the ICT profession from other careers.

Mr Vicnan Pannirselvam, a regional marketing analyst with Uber comes from a rather unconventional background for a data specialist. He was trained in media and communications and then studied philosophy, politics and economics. But not having formal academic training in data science and analytics did not stop him from embarking on a career in this field.

As a self-starter, he had proactively pursued new knowledge and skills to chart his career towards his passion in data science. He had used his SkillsFuture Credit to pay for a Coursera certification in “Data Science at Scale”, which he recently completed, and he is currently pursuing a part-time Master’s degree in Systems Thinking. 

The Skills Framework would benefit someone like Vicnan, as it can guide him for his career progression by revealing where the possibilities lie and what skills would be useful for his career advancement, especially for a relatively nascent profession.

Another example is Ms Wong Ching Yee, who used to work in the chemical engineering industry. She made a mid-career switch to healthcare nine years ago, after completing a Nursing diploma course at Nanyang Polytechnic. She is currently a Nursing Informatics Applications Specialist with Farrer Park Hospital.

Her role requires her to integrate nursing knowledge with advancements in information technology, to improve service quality in her organisation.  As an advocate for life-long learning, she attends seminars and workshops to keep abreast of technological trends such as Artificial Intelligence, which she believes can be deployed in a meaningful way to improve patient safety. 

The Skills Framework could also benefit someone like Ching Yee, who wishes to expand her perspectives on emerging skills that are in demand, so that she can up-skill herself to seize new opportunities in her career. 

Co-Developing Skills Framework for ICT with the Industry 

The Skills Framework for ICT is co-developed by the government and industry. We had extensive consultations with more than industry leaders across different sectors and company profiles, including multinational companies, large local enterprises, small-medium enterprises, start-ups and training providers. This is to ensure that the Skills Framework for ICT is industry-relevant and remains versatile for evolving technology and across industries. 

The Cyber Security Agency, or CSA, was involved in jointly developing the Cybersecurity track of the Framework, to provide guidance to industry and academia on Cybersecurity skills development. I am pleased that CSA had referenced the Skills Framework for ICT and went further to develop its own Cybersecurity Competency Framework, to provide guidance on the professional growth of Cybersecurity officers in the Public Service.

I would like to take this opportunity to thank the early adopters of the Skills Framework for ICT, and all those who have participated in the TeSA Governing Council, Sector Committees and the Skills Framework Advisory Panel, for developing and supporting its implementation.

Technology trends and ICT skills are ever changing. The Skills Framework for ICT is in permanent beta state and we will continue to work with the industry to iterate and enhance it, to ensure its relevance in the fast evolving ICT landscape. 

Conclusion

In resource-scarce Singapore, talent is the key driver of growth. This similarly applies to the ICT sector. Our Government will do all we can to prepare individuals and businesses to harness the growth opportunities in the digital economy. 

I would like to encourage companies, professionals and students to fully utilise the Skills Framework for ICT to your advantage, be it for developing ICT talent or planning for your career path. Thank you very much.