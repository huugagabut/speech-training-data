Working towards a digital vision for Singapore, with a world-class infocomm media sector empowering other industries and improving people’s lives

Digitalisation as a key driver of growth going forward

Given the quickening pace of digital innovation worldwide, we need to accelerate our own digital transformation to prepare our people and our businesses for the future. We need to develop strong digital industries in their own right, and we need to catalyse their transformation of other industries to spur productivity and yield new synergies.


Last October, the Infocomm Media Development Authority, or IMDA, was formed after the restructuring of two statutory boards. Eight months on, IMDA has taken important strides in driving a digital vision for Singapore – a vision where we have a world-class infocomm media sector empowering other industries and improving people’s lives. 


Building blocks that IMDA has laid to drive the digital economy

A key enabler of our digital vision is pervasive technology that is fast, reliable, and secure. Over the years, IMDA and its predecessor has built up and enhanced Wireless@SG so that more people can more easily work, play, and connect with others on the go. For example, last month, IMDA has made available automatic login for non-SIM devices including tablets and laptops. With this enhancement, you can connect to Wireless@SG more conveniently, without having to key in your username or password.


IMDA has also been actively investing in our people to skilfully and creatively seize digital opportunities. Since IMDA launched the TechSkills Accelerator or TeSA in March 2016, more than 110 SkillsFuture Career Advisors have come on board and more than 10,000 professionals have benefited from TeSA’s programmes to acquire ICT expertise and skills. An example is Ms Masturah Maidin, age 27, who wanted to upskill from being a graphic designer. She took up General Assembly’s User Experience Design Immersive course under TeSA’s Tech Immersion and Placement Programme, and now, she works as an UX designer at EON Reality. Going forward, IMDA, SkillsFuture Singapore, and Workforce Singapore aims to launch a Skills Framework for ICT professionals at the end of this year.


To support our people in lifelong learning and continuous skills development, in line with SkillsFuture, IMDA has also been working with our Post-Secondary Education Institutes to develop industry-relevant skills-based modular courses for skills training in areas of high demand. Skills-based modular courses are shorter in duration compared to a diploma or degree, which makes learning more accessible for working adults, and is a key advantage especially in a fast-changing ICT industry. For example, NUS and NTU will be introducing a suite of modular courses in Cybersecurity, Software Engineering, and Business Analytics this year. These modular courses can be stacked towards Undergraduate Certificates, Specialist Certificates, or a relevant part-time degree at NUS and NTU.


In addition to developing skills for the digital economy in our people, IMDA has expanded efforts to enhance the digital readiness of SMEs, which hire 70% of our workforce and contribute to nearly half our GDP. In March this year, I announced the “SMEs Go Digital” programme, which aims to raise SMEs’ overall level of digital readiness by giving them step-by-step advice on digital technologies to use at each stage of their digital journey. 


Since then, I am happy to hear that more than 50 digital solutions have been approved by IMDA. These solutions can help our SMEs to improve operational efficiency and enhance customers’ experience. For example, a QR code technology developed by local SME veriTAG helps to authenticate local exports to China. Customers in China are able to check the authenticity of the product by simply scanning a QR code on the product. With this solution, users are also able to collect data, gain insights into the sales situation at various locations, and plan their marketing and sales activities accordingly. 


In addition to making pre-qualified solutions available to SMEs, the “SMEs Go Digital” programme will enable SMEs to receive specialist advice on areas such as cybersecurity, data analytics, and data protection at the SME Digital Tech Hub, which is slated to open by the third quarter of this year. SMEs can also look forward to obtaining step-by-step guidance on technologies to use at each stage of their growth through the sectoral Industry Digital Plans, which are aligned to the national Industry Transformation Maps. We plan to roll out the first Industry Digital Plan, which will be for the retail sector, later this year.


Aside from helping SMEs reap the benefits of the digital economy, IMDA has also been supporting the scale-up of promising tech companies in Singapore via Accreditation@IMDA. Over the years, Accreditation@IMDA has helped companies establish credentials, build track records to compete in the global market, and facilitate fund raising for expansion. To provide runway for further growth, Accreditation@IMDA is now also working with SGX to increase the accessibility to capital markets for tech companies. More details about this partnership will be announced at a joint event with SGX next week. 


Developing four frontier technology focus areas to lay a strong infocomm media foundation for the nation

Beyond developing infrastructure, manpower, and other enablers for our digital vision, we also need to build a strong infocomm media foundation to support Singapore’s digital transformation. To build this foundation, IMDA has identified four frontier technology focus areas to invest and build capabilities in. They are exciting fields with bright prospects in their own right, and they have great potential to transform other industries and enhance people’s lives.


The first focus area is Artificial Intelligence and Data Science. At InnovFest Unbound three weeks ago, I mentioned the establishment of AI.SG, a new national programme with funding of up to $150 million, to boost Singapore’s AI capabilities. Through partnerships between research institutes and companies to expand knowledge, create tools, and develop talent in AI, we hope that AI.SG can help to address major challenges that affect society and industry, in areas such as finance, city management, and healthcare. Another initiative I announced at InnovFest Unbound is the establishment of the Singapore Data Science Consortium. This will strengthen collaboration among Institutes of Higher Learning, research institutes, and the industry in data science R&D, with the aim of facilitating industry adoption of cutting-edge data science and analytics technologies to address real-world challenges. The consortium will also build a local pipeline of talents with deep data science capabilities.


In addition, under TeSA’s Company Led Training programme, we are happy to announce the first TeSA Fintech project with DBS Bank to develop more fresh professionals with capabilities in Agile Development, Development Ops, Information Security, and Data Analytics for the financial services sector. We would also like to welcome new partners such as Tata Consultancy Services and DerivIT, which have recently come on-board to train fresh professionals in fintech, data analytics, and software development. Riding on its past success in training data analytics professionals, IMDA has also partnered SAS Institute to embark on a fourth Company Led Training programme to develop more data science and analytics professionals for the industry.


The second focus area is cybersecurity. I don’t think I need to belabour how important this is for your companies from operational, financial, reputational, intellectual property, and other angles. It is important as well for all of us here as individuals, and it is essential for our national security. I am thus pleased to hear that companies like Singtel, ST Electronics (Info-Security), Quann, Accel, and Deloitte have embarked on our Cybersecurity Associates and Technologists, or CSAT programme to train more cybersecurity professionals for the industry. The government has also established the National Cybersecurity R&D Programme since 2013, with funding of up to $130 million over five years, to develop R&D expertise and capabilities in cybersecurity to improve the trustworthiness of Singapore’s cyber infrastructure.


The third focus area is immersive media. Virtual and augmented reality technologies can be used across various industries to improve processes, reduce costs, and enhance outcomes. Let us pause for a moment to imagine the possibilities of immersive simulation and augmented visualisation in retail, architecture, construction, healthcare, defence, and more. 


Today I will speak in particular about the great potential of virtual reality, or VR, to enhance the educational experience of students. IMDA has worked with production company Beach House Pictures to pilot the use of VR in Social Studies in five primary schools here in Singapore. Through the pilot lessons, about four hundred students in Primary Four and Primary Five learnt about design and architectural features of buildings as well as characteristics of high-tech farming. Also in this pilot, Beach House Pictures worked with MOE to develop exciting VR content closely aligned to the Social Studies curriculum. Beach House Pictures has also collaborated with local start-up Hiverlab to develop a customised VR classroom application for our teachers to guide students on VR experiences to different locations around Singapore.


As another example of using immersive media for education, IMDA and one of our hospitals, Tan Tock Seng Hospital will be collaborating with local tech and visual effects company SideFX Studios to use VR and mixed reality to augment clinical training. Traditional clinical training is resource intensive and costly, and might not be able to replicate scenarios realistically. This collaboration will develop immersive simulations for basic surgical skills and complex airway management, which is critical in life-threatening emergencies. 


The government has also established International Research Centres under the Interactive Digital Media Strategic Research Programme, some of which have developed strong R&D capabilities in VR and augmented reality, or AR. These include the BeingTogether Centre hosted at NTU that focuses on immersive telepresence technologies, such as the Keio-NUS Connective Ubiquitous Technology for Embodiments (CUTE) Centre hosted at NUS that focuses on feeling communication through multi-sensory connection technology.


The fourth focus area is the Internet of Things, or IoT, and future communications infrastructure. Exponential growth in processing power, internet connectivity, and usage of mobile devices have led to rapid growth of devices, services, and software for IoT worldwide. To further multiply the potential of IoT, and to further fuel the digital economy, IMDA will be partnering the industry to develop and put in place key components of future-ready and resilient communications infrastructure that will benefit consumers and businesses across various sectors. Components of the infrastructure include enhancements to the Nationwide Broadband Network, IoT networks, 5G mobile network, and sensor networks. These will enable businesses to leverage high-speed networks, real-time communications, and high-accuracy location positioning to better deliver their services. 


To ensure that these infrastructural plans will address and meet the industry’s needs, IMDA will be conducting industry consultations to seek feedback and ideas. In fact, the first set of consultations will kick off later today. IMDA will be seeking your views on 5G spectrum requirements and regulatory provisions, and on how policies can move in tandem with technology and more importantly, address your needs. IMDA will also waive frequency fees for 5G trials to lower the regulatory barrier and to encourage industry trials in 5G technology. 


Improving people’s lives through Singapore’s digital vision

I mentioned earlier that our digital vision for Singapore is to have a world-class infocomm media sector empowering other industries and improving people’s lives. I have spoken about our four key frontier technology focus areas for developing a vibrant and strong infocomm media sector, and about IMDA’s efforts in developing a strong talent pool via TeSA and in enhancing the digital readiness of SMEs via SMEs Go Digital. But ultimately, we hope that all our efforts to prepare Singapore for the digital future will improve people’s lives. 


Therefore, amidst efforts to build a strong digital economy, my ministry is also driving a national effort to nurture a digital society where all Singaporeans, young and old, have access to technology, understand it, and benefit from using technology in their everyday lives.


There are many ongoing efforts, and I would like to make special mention of IMDA’s PlayMaker Programme. This began in 2015 as an effort to acquaint our young children with technology through play. Using technology-enabled toys, the programme promotes a tactile and kinaesthetic learning experience for preschool children. Guided by adults, children can acquire abilities like logical thinking, reasoning, sequencing, estimation, and inventive thinking. As the toys encourage collaboration, children can also develop social and communication skills. The programme has since been rolled out to 160 preschool centres in Singapore, and has benefitted 10,000 students with its special brand of learning. Last week, the PlayMaker Programme was awarded the Bronze medal in IMS Global’s annual Learning Impact Awards. This annual competition, currently in its 11th year, is conducted on a global scale to recognise outstanding and influential uses of technology to address the most significant challenges facing education. Past years’ winners include our Ministry of Education in 2009, the Netherlands’ Ministry of Education in 2016, overseas universities, and companies such as IBM and Marshall Cavendish. Congratulations to our PlayMaker team for making us proud!


Amidst rapid technological changes, another key priority is ensuring that our people can continually be gainfully employed. To help displaced PMETs, IMDA has been working with partners like Workforce Singapore, or WSG, Singapore Computer Society, e2i, and NTUC on initiatives such as TeSA Integrated Career Services, programmes and courses for upskilling and reskilling PMETs, and new skill upgrading pathways. 


In particular, IMDA has been actively working with WSG and companies to help local displaced PMETs embark on new ICT careers through Adapt and Grow initiatives, such as Professional Conversion Programmes for Data Analysts and Full Stack Software Developers. For example, one of IMDA’s SMEs Go Digital partners, Aptiv8, is in urgent need of skilled professionals to scale up their roll-out of digital solutions to SMEs, and they are keen to tap on some of our PMETs who have been displaced. At the same time, we recognise that there could still be skill gaps among these PMETs. To overcome this, IMDA and WSG are working closely with Aptiv8 to provide conversion training opportunities for displaced PMETs through TeSA. I would like to applaud Aptiv8 for helping SMEs go digital and supporting displaced workers at the same time. We need more partners like Aptiv8, and I hope to see more companies coming on board.


Conclusion

In conclusion, the infocomm media sector holds great promise to make a huge difference in powering Singapore into its next phase of growth. Let us work together to build a world-class infocomm media sector in Singapore that can empower other industries and improve people’s lives.

