A Digital Future for all Citizens

Mr Chairman, like the rest of the world, Singapore is headed towards a digital future. It is MCI’s mission to ensure this digital future is also a brighter one, for all Singaporeans. 

In my speech, I will address three key strategies to realise this goal. First, help citizens get more out of the digital economy by improving last-mile delivery infrastructure. Second, strengthen social cohesion by making trusted information widely available. Third, help all citizens to be digitally connected.  

Transforming Last-Mile Delivery Infrastructure 
 
Mr Mohamed Irshad asked about disruptions and future prospects for post, as well as parcels. Members would recall SingPost had a number of service lapses in 2018 and early 2019. We are in a different place this year. SingPost has been improving service delivery since 2019 to rebuild public trust. They have extended delivery hours for peak periods, improved staff remuneration and hired more postmen. Consumer complaints fell by about 40 per cent in December 2019 compared to 2018. Consumer satisfaction has also grown. SingPost’s stronger performance reflects the efforts they have put in. MCI and IMDA will continue to work closely with SingPost to enhance its service delivery.

Mr Irshad also asked about the prospects of the postal industry. In Singapore, the popularity of e-commerce has led to a steady increase in parcel deliveries. But given Singapore’s urban context and consumers’ busy lifestyles, doorstep deliveries often mean missed deliveries. Our postal infrastructure must evolve to offer practical alternatives to doorstep deliveries. 

Building on the success of the Locker Alliance pilot, IMDA will launch a nationwide deployment of 1,000 locker stations in HDB estates, MRT stations and Community Centres by end 2022. This will complement existing commercial locker stations and doorstep deliveries. The goal is to place a locker station around five minutes’ walk from every HDB block.  Users can collect parcels on their way to work, or on their way home, any time of the day. Merchants and logistics service providers will also enjoy greater delivery efficiency. This may result in more competitive delivery prices for consumers.

IMDA has received strong support from major e-commerce marketplaces like Qoo10 and Shopee, and logistics service providers like Qxpress. IMDA will also partner with SingPost to leverage its extensive postal service experience. 

A Cohesive and Engaged Society with Trusted Information

As we strengthen infrastructure, we must also safeguard our social cohesion. We believe all citizens should have access to trusted information, and we do this in two ways. First, our media industry creates compelling content for audiences. MCI supports the growth of the media sector so it stays ahead of the curve. Second, our government communications are always centred on citizens. MCI varies the modes of engagement, using face-to-face and multilingual communications to connect with all Singaporeans.

Let me cover these areas in turn.

Quality media content to engage all Singaporeans
   
The media industry plays a significant role in keeping citizens well informed. Mr Darryl David asked how the Public Service Broadcasting (PSB) can continue to be relevant. PSB has evolved alongside citizens’ consumption preferences by providing customised content across multiple platforms. For example, Mediacorp’s vernacular programmes are taking a multi-platform approach. To widen audience engagement, Vasantham brought the Deepavali Countdown Show 2019 directly to audiences in Little India. Viewers were able to celebrate in Little India with the “live” show, and catch the telecast on meWatch and Vasantham’s social media platforms. Tech-savvy younger audiences can also enjoy more online content tailored for them. IMDA is partnering popular digital platforms such as The Smart Local and Viddsee to produce a wide range of drama programmes and documentaries, which will be available this year.

Mr David also asked about plans to develop the media industry amid competition from over-the-top (OTT) media services, such as Netflix. Grooming creative talents is key. The Skills Framework for Media maps out career pathways and skills in emerging trends such as immersive media, to help media professionals stay relevant. Mentorship programmes like WritersLab, ProducersLab and the Story Lab Apprenticeship also help media professionals sharpen their tools of the trade. These programmes will benefit over 200 media professionals over the next two years.

IMDA is also helping local media content go global. I am glad to see the rise of “made with Singapore” content in recent years. One example is the drama series Food Lore by home-grown company Bert Pictures and prominent directors in Asia. The drama has shone on the international stage and done us proud. Through initiatives like the Capability Partnership Programme, IMDA will continue to collaborate with global players such as Facebook and CJ ENM Hong Kong, and strengthen the expertise of local media companies.

Last-mile engagement to connect deeply with citizens

Ms Tin Pei Ling asked how the Government ensures timely access to information for Singaporeans. This brings me to my second point on citizens. We are strengthening last-mile engagement to connect with citizens face-to-face. Strong community partnership amplifies these efforts. For example, 3,000 Silver Generation (SG) Ambassadors actively engage seniors with regards to the Merdeka Generation Package (MGP). One of the SG Ambassadors is Mr Kong Seet Kiang. Already 70, he has been volunteering three days a week since 2015, and is a familiar face amongst Bukit Timah seniors.

MGP roadshows are also in full swing in the heartlands. With the deep involvement of the community, the Government has engaged nearly 200,000 MG seniors and their families at over 200 roadshows and events. 

A new level of ambition for WOG translation

Good translation is essential for important messages to reach all segments of Singapore society. MCI’s Translation Department (TD) undertakes the most important pieces of translation work for the Government. It also champions high standards when it comes to translated work produced by all public agencies, whether done in-house or outsourced. 

Over the years, MCI initiated various moves to boost our translation capabilities, producing increasing quantities of translated materials, at good quality, and at speed. MCI is now ready to consolidate these moves. We will signal a higher level of aspiration, by repositioning MCI’s TD as the Whole-of-Government Centre of Excellence for Translation. 

The Centre of Excellence will drive three key workstreams: talent, technology and partnerships. First, we will groom translation talents and upskill industry practitioners. This is done through programmes like the MCI Information Service (Translation) Scholarship and the Translation Talent Development Scheme.

Second, we use technology to improve the speed and quality of government translation work.  In the national fight against COVID-19, our machine translation engine, known as ‘SG Translate’, has been supporting the translation of public communications materials in vernacular languages. As ‘SG Translate’ produces translations that suit the local context, it is more accurate than other machine translation tools. Mr Teo Ser Luck asked how we are involving more partners to enhance translation technology. We fully agree partnerships are important to share knowledge and expertise. That is why MCI is launching a new pilot project, ‘SG Translate Together’, to rally Singaporeans to improve ‘SG Translate’. From 2021, MCI will invite selected groups, including businesses and schools, to submit quality translations through a web portal. The more quality inputs we receive, the more data for training the engine, and the stronger the technology’s underlying AI will become.We will open up the web portal progressively to more users. This will benefit more practitioners while also growing the engine’s capability.

Third, MCI is also strengthening partnerships to close translation gaps. For example, we are working with organisations with skilled translators to meet periodic surges in translation demand within the public sector. We also have retainer arrangements to boost vetting capabilities. 

A very big thank you to our translation partners. There is so much more we can do together. Let’s continue our partnership to ensure strong government communications for citizens.

Understanding Our Past, Reimagining the Future

If translation is the bridge to connect our multiracial communities, then preserved records open a window into Singapore’s shared heritage. To strengthen cohesiveness, we must remember the shared history that binds us.

The National Library and the National Archives of Singapore (NAS) are widening public access to national and social memories. At the ongoing Legal Deposit display at Bedok Public Library, Singaporeans can access a vast array of publications from yesteryear. One of them is the book ‘Assembly Songs’, a valuable compilation of school songs of the past. In total, the Legal Deposit collection has more than 1.37 million items contributed by publishers. 

With the updated National Library Board (NLB) Act empowering NLB to archive digital materials, NLB has also added 40,000 Singapore websites to its digital collection, preserved for generations to enjoy. The NAS will be launching a crowdsourcing initiative to capture treasured sights and sounds of Singapore. Citizens can play a part by contributing under categories such as ‘Sounds of the Heartlands’ and ‘Festivals and Celebrations’. 

Anchored by our past, we can face the future with confidence.  

Strong partnerships to build digital inclusivity  

Technology can empower our people. However, some segments of the population, such as seniors and low-income households, may feel lost in the digital society. They may struggle to catch up. I understand their anxiety. Singapore must therefore focus on digital inclusion. We must overcome age, income and literacy gaps, so everyone can reap the benefits of the digital economy. 

Mr Vikram Nair asked about measures to boost digital readiness, while Dr Teo Ho Pin asked how we are improving digital access for low-income households and seniors. NLB and IMDA are galvanising the community and industry to build digital literacy and skills. This is in sync with the Singapore Together movement, where the Government partners with Singaporeans to build a better future. 

To support low-income Singaporeans, the Home Access Programme has subsidised broadband for over 14,000 households. One of the beneficiaries is Mr D Rashpal Singh Sidhu. With subsidised broadband, Mr Sidhu found a part-time job after searching online. He also uses the Internet to stay in touch with overseas relatives. 

However, while household broadband access in Singapore has increased over the years, some low-income households are still unconnected.  To benefit more low-income households, IMDA will enhance the Home Access Programme from April. IMDA will partner with M1, MyRepublic Ltd and NetLink Trust to defray the cost of broadband for low-income households, while offering faster broadband speeds. IMDA will also offer a wider range of devices, giving households the choice of either a subsidised smartphone or tablet. We aim to benefit 10,000 more low-income households over three years.  

We are also helping seniors to learn digital skills. IMDA, together with partners such as NLB, organised over 200 free Digital Clinics in libraries and community spaces to help seniors with their smartphone devices. Some 3,000 volunteers have reached out to over 15,000 seniors island-wide. One of these seniors is 64-year-old Madam Safia bte Mohd Salleh. Through Digital Clinics, she discovered useful apps to make polyclinic and hospital appointments, and check the arrival times of buses. She plans to visit Digital Clinics again to learn more. 

There are also ground-up efforts to help children navigate the online environment safely. This is important as they are increasingly exposed to technology from an early age. The Media Literacy Council is partnering Google to bring a mobile interactive exhibition on online safety to primary schools this year. Students can learn about online safety, cyber-bullying and the actions to take when encountering problems online. Our Singapore Fund for Digital Readiness also supports community efforts to combat online risks. For example, a group of Nanyang Technological University students organised an exhibition to raise awareness of online child grooming. Visitors could step into the shoes of victims through interactive installations. I visited the exhibition in NLB last week. It was well done, and the messages hit home.

I applaud the strong contributions of the community to promote online safety.

Connecting Singaporeans with inclusive social spaces

Our march towards the digital future must also be matched by an unwavering sense of community. In the fast-moving digital world, it is more important than ever to connect people through common spaces and programmes. Our libraries play a key role in this. They have transformed over the years to become trusted and treasured spaces where people can bond, socialise and learn. 

Mr Cedric Foo asked how we are revamping libraries to ensure they stay relevant in a digital world. We have revamped six libraries under the Libraries of the Future initiative. Strong community engagement to encourage lifelong learning is the hallmark of these libraries. 

For example, at library@harbourfront, which opened last year, citizens young and old can learn about emerging technology at The Tech Showcase. Interactive displays on topics like educational robotics offer an engaging learning experience. Families can also participate in hands-on activities at the children’s makerspace. The revamped libraries have seen an increase of 73 per cent in total visitors and 49 per cent in total loans in the first year of operation. Our libraries are an exception to the global trend of falling library usage. 

We are continuing our libraries’ transformation. NLB will revamp eight more libraries from now to 2026. They include Choa Chu Kang Public Library, Central Public Library, Queenstown Public Library and Marine Parade Public Library. In addition to these, a brand new Punggol Regional Library is in the works. 

These future libraries will play a stronger role in connecting Singaporeans. They will build inclusive learning communities.  For example, Punggol Regional Library will offer spaces and collections for all age groups and needs. Users with disabilities and children with special needs can enjoy customised services. The library will also house a specially-curated world children’s literature collection so younger Singaporeans can appreciate cultural diversity from an early age.

Conclusion

Mr Chairman, the digital future presents endless possibilities.  MCI will persevere in our efforts to ensure every business, worker and citizen can seize digital opportunities. We will continue to nurture enduring partnerships to realise this vision, and leave no one behind.

Let us work together, as one nation, to forge a strong digital future.
