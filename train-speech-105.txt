INTERCONNECTNG ASEAN’S DELIVERY NETWORKS IN THE DIGITAL ECONOMY

E-Commerce as a Key Growth Driver for Postal

The global economy is facing profound transformation due to technological developments. The postal sector is no different.  In our own region, Malaysia’s on-demand delivery start-up Zoom, Indonesia’s crowd-sourcing logistics start-up Etobee, and Singapore’s logistics start-up Ninja Van are all examples of businesses that have harnessed digitalisation to challenge traditional delivery players.

In tandem with the rise of the Digital Economy, e-commerce has flourished and become a key driver of growth for the postal sector.  In particular, Southeast Asia is seen as the next frontier for e-commerce, with our growing middle class and more than 100% mobile penetration rate in most markets.  Global leaders are making early moves to secure a piece of the pie in ASEAN.  Amazon has launched its largest global Prime Now Facility in Singapore, while Alibaba has partnered Malaysia on the Digital Free Trade Zone.

With their extensive reach regionally and internationally, postal operators are well-positioned to serve as the logistical backbone of e-commerce.  Those who have already seized the opportunity have benefited immensely.  For example, Spanish postal operator, Correos, launched their Tmall global shop last year.  All the products sold in their shop come from Spain.  In doing so, Correos reinvented itself from being a mere delivery service provider into an active fulfilment partner.  It enabled SMEs in Spain to access a larger international market, and carved a niche for itself in Spain’s digital economy. 

Re-imagining Postal Infrastructure

There is no one-size-fits-all strategy for transformation.  Each organisation has to find unique solutions to complement its structure and circumstances.  SingPost’s traditional business in postal services has also been disrupted, and the company has responded by harnessing technology intensively. 

Today, traditional post offices in Singapore are undergoing a transformation into Smart Post Offices, with self-help kiosks to enhance efficiency at the collection point.  Technology is also deployed for fast and accurate sorting of parcels.  This is complemented with Track and Trace solutions to give customers better sight of the whole delivery process.  I congratulate SingPost on winning the 2018 World Post and Parcel Awards for Technology. This is a strong affirmation of its digitalisation efforts, and it is only the beginning.

But to thrive in the Digital Economy, we must go further, beyond raising service quality and improving operational efficiency, to rethink and re-imagine the postal infrastructure, especially for last mile fulfilment.  There are no easy solutions.  Even advanced cities like Tokyo, London and Zurich are wrestling with different strategies to manage the last mile. 

In Singapore, we are building a network of conveniently located lockers – such as PoPstations by SingPost and Blu lockers.  This is a solution that is also being tested out in Japan.  The lockers not only give customers the flexibility to collect and drop off parcels at any time of the day, they also enable post and parcel organisations to deliver much more efficiently and at lower costs overall.  We are rolling out the locker network in phases, with the aim of achieving a nationwide coverage. This is only our first step towards transforming the postal and logistics infrastructure in Singapore, as we grapple with the new reality of e-commerce and B2C parcel delivery, manpower constraints, and traffic congestion. 

Inter-Connecting ASEAN’s Delivery Networks

While each postal operator embarks on its own transformation, it is also important that we work closely together and draw on each other’s strengths.  With ASEAN’s large geographical area and diverse cultures and languages, building a single uniform delivery network across the region would be a tall order.   

This is an opportunity for ASEAN postal operators and other players to ride the e-commerce wave not just locally, but also regionally.  We should leverage each other’s delivery networks and local expertise to enable seamless last-mile e-commerce delivery across the region, and further integrate the ASEAN economy.  For a start, we can explore inter-connecting local delivery networks into a regional alliance of collection points or lockers.  SingPost recently launched a new Last Mile Platform to integrate last-mile delivery services in ASEAN, and is keen to connect their platform to such a regional alliance. In that regard, I would like to encourage ASEAN postal operators to come together and actively explore this proposition of inter-connecting ASEAN’s delivery networks. 

The Role of the Postman

Before concluding, I wish to highlight the important role played by the hardworking men and women who deliver our letters and packages. We understand the changes faced by the postal workforce in the digital economy.  In this regard, I am heartened by SingPost’s continuous efforts in helping its staff to upskill and adapt, such as rewarding staff for upgrading their skills and competencies through the SingPost Inclusivity Fund. 

Nevertheless, our postmen play a larger role beyond driving efficiency in delivery.  They also play a critical role in the public’s experience of the postal service.  When a postman goes the extra mile to accommodate changes in delivery address or timing, the customer remembers.  Hence, I encourage our postmen to persevere with service excellence and uphold strong ethos in serving the community.  Because even as we go digital, our people are the custodians of the public’s trust and the face of postal services.
