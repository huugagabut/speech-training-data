We are familiar with the bright side of cyber technology. Internet connectivity is creating new business opportunities and raising our quality of life. But there is also the dark side of cyber; these are the computer hacks, data breaches and online scams that endanger cyberspace. Many of you would have heard about the recent Distributed Denial of Service or DDoS attacks on Dyn, a US-based Domain Name Server (DNS) provider which caused popular websites in the US to be inaccessible to users there. Closer to home, StarHub also reported DDoS attacks on their DNS servers just two weeks ago, which resulted in service disruptions to its subscribers.

The threat of cyberattacks is real and present, and affects all of us. The Cyber Security Agency was formed last year to take the lead in addressing these threats and just last month, the Prime Minister launched the national cybersecurity strategy. It calls upon all stakeholders – Government, the cybersecurity community and businesses – to participate in building a resilient cyber environment.

More industry-ready graduates
 
The strategy outlines how the Government is stepping up national efforts to protect critical information infrastructure, strengthen international cooperation, and, importantly, to develop cybersecurity manpower.

Today, cybersecurity professionals are in demand. The 2015 IDA Infocomm Manpower Survey estimated a current shortfall of 1,000 professionals. This shortfall is expected to grow to 2,500 in the next three years, with SMEs making up 80% of this demand.

The Government has taken steps to close this manpower gap. All our university and polytechnics now offer cybersecurity programmes. By end 2018, we expect more than 800 cybersecurity degree and diploma holders each year. But more can be done to help these students join the workforce. I am happy to announce that the Cyber Security Agency has signed MOUs with the Singapore Institute of Technology and Nanyang Polytechnic. CSA will collaborate with these two Institutions of Higher Learning on skills development and new initiatives that help Singaporeans build up cyber resilience. One key outcome of these MOUs is that students would be more industry-ready upon graduation.
 
Cybersecurity is a collective responsibility

The Government is doing more to create a safer cyberspace, but cybersecurity is also a collective responsibility where all of us, businesses and individuals, must play a part. We need to fight the dark side of cyber together.

I am encouraged to see our businesses pay attention to cybersecurity for three reasons. First, cybersecurity is a corporate responsibility. No business is too small to be hacked and used in cyber-attacks. In fact, small businesses offer a perfect cover for attackers. One example is Cate Machine and Welding, an American family business in rural Wisconsin. They had a simple back-office computer. Little did they know that it had been commandeered by hackers to stage cyber-attacks against targets across the world, including a major Manhattan law firm, one of the world’s biggest airlines, a prominent university and others organisations across Thailand and Malaysia. Thankfully, this was discovered by a cyber threat intelligence company. 
 
In the DDoS attack on Dyn that I mentioned earlier, hackers took over devices as innocuous as Internet-connected CCTVs to generate massive amounts of traffic that overwhelmed critical Internet infrastructure. In the StarHub incident, consumer devices were also part of the affected infrastructure. As corporate citizens, businesses should ensure that their electronic devices are secured against hacks. Some of the simple steps you can take today are changing the default passwords of routers and Internet-connected devices, as well as updating your software. More importantly, businesses that add Internet connectivity to their products must build in cybersecurity controls, and not let their products be hijacked for attacks.

Cyber threats are an enterprise risk
 
Second, cyber threats are an enterprise risk that can incur operational and reputational costs. Rokenbok is a not-for-profit toy company in California. Last year, it was attacked by ransomware just before the holiday shopping season. Its company files were encrypted and rendered unusable. In two days, the company lost thousands of dollars in sales. This was not the first time it was attacked. Its website had once been shut down by a DDOS attack.

Many Singapore organisations have also experienced cyber-attacks. In the first half of 2016, there were more than 1,300 reported cases of Web defacements. Most of the affected websites belonged to SMEs across a range of businesses, from interior design to manufacturing. We also saw an increase in the companies affected by ransomware. CSA received 17 cases of ransomware this year, up 8 times from 2 cases in 2015. The figure is likely to be higher as companies do not always report these cases. The lesson for all of us? Take action today. Don’t pay tuition fees to cybercriminals.

Business leaders have to invest in cybersecurity. To manage cyber risk at an enterprise level, business leaders should be aware of the cost of consequences. How will a breach affect your operations? How will it affect your reputation? Inevitably, you will have to make trade-offs between security, cost and usability. These trade-offs should be based on well-informed and conscious decisions at the management level, and not left to external vendors or the back office. Thus, security should be factored into ICT procurement decisions, as one would factor in cost-effectiveness and other critical factors.
 
To maximise the security returns of your investment, it is important to adopt a security-by-design mindset. This means incorporating cybersecurity and data protection during the development process and not as an afterthought. To maintain the value of your investment, you will also have to continuously keep your cybersecurity measures up-to-date, for example, by updating your anti-virus software and maintaining your inventory of devices and software.

Cybersecurity makes good business sense
 
The third reason that cybersecurity is important to businesses is that it makes good businesses sense. In this digital age, companies need to assure their customers that digital assets are properly handled and comply with the legal and contractual requirements for digital asset protection. SMEs looking to strengthen their cybersecurity and data protection capabilities by adopting international standards like the ISO 27001 for Information Security Management can tap on government schemes, such as SPRING Singapore’s Capability Development Grant, or CDG, which may be used to defray up to 70% of qualifying project costs.
 
One company that benefited from the grant is SecureAX, a managed cloud computing and virtualisation provider with operations in Singapore and Thailand. In the past, it adopted its customers’ IT security policies, leading to resource-intensive monitoring of individual IT policies. By attaining the ISO 27001 certification, it was able to gain the confidence of more customers as the standard ensures that sensitive information remains secure across people, processes and IT systems.  The company has been able to expand its customer base and is now looking to have its Thailand office adopt the similar framework.  
 
Other than international standards, the Personal Data Protection Commission facilitates SMEs’ upgrading of their data management capabilities, which may also be supported under the CDG. With such support in place, I hope to see more companies secure their digital assets and be ready to address fast-changing cyber-threats.

Trade Associations and Chambers can support their members in the cybersecurity journey
 
To help businesses raise their level of cybersecurity, CSA will work closely with the Trade Associations and Chambers, leveraging their ability to reach out to members and customize resources for their sectors.
 
I am encouraged by the Singapore Business Federation’s (SBF) efforts to raise cybersecurity awareness and thank SBF for its support. SBF has featured cybersecurity in the National Security Conference over the past three years. This year, it even brought in cybersecurity service providers, so that you can take action and discuss possible solutions today. I am glad to see telcos represented among the providers, because cybersecurity should be a part of a holistic package of Internet services.

I am also pleased that SBF has built on the conference by launching a series of Cybersecurity Capability workshops. These workshops help companies with no technical background to understand the risks, plan mitigation measures and conduct their own cyber drills.

I encourage all Trade Associations and Chambers to play a part in raising their members’ cybersecurity, and businesses to participate in these initiatives.

In conclusion, cybersecurity is a collective responsibility. By keeping our own systems safe, we help others as well as ourselves. Businesses and trade associations have made an encouraging start, but we must move faster to strengthen our cyber resilience. Cyber security is a team sport – let’s all cooperate and “fight the dark side together”. Thank you.