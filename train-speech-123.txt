TeSA Health Tech Day.

Technology enables healthcare transformation and provides opportunity

Like many other parts of the world, Singapore’s population is ageing. By 2030, one in four Singaporeans will be 65 or older, double the number today. While this reflects positively on our healthcare system and our overall living environment, an ageing population does pose some challenges. In the healthcare sector for example, it means that we will have a smaller pool of healthcare workers who have to look after a larger group of elderly.

Technology is a key enabler in tackling this challenge and making our healthcare system more productive and future-ready. It offers new possibilities. If we can innovate new solutions to tackle our healthcare challenges, we can provide better care for our people and create economic opportunities for our companies to export their solutions to other cities around the world that are also experiencing similar demographic shifts.

We are thus supporting innovation in health tech development and adoption. The exhibits here show that we have made a good start and we can build on this momentum. For example, IHiS is organising the inaugural National HealthTech Challenge, to create a more systematic, scalable and sustainable approach to health tech innovation. The Challenge will bring together healthcare professionals, research institutes and the tech industry, to identify pressing healthcare challenges and co-create solutions through technology. We hope to support up to ten promising solutions for a start, and grow this over time.

Bringing through talent from other sectors

To support innovation, we must also grow and deepen our pool of health tech talent. We are doing so in two ways. The first is by bringing in talent from other sectors.

Tech workers today are highly mobile, as tech skills can be applied to many industries. Many of our health tech professionals come from other sectors. Mr Vincent Seah is one example. Vincent studied Business Administration in university. After graduation, he worked in a financial sector MNC’s Risk and Assurance team for many years before joining IHiS. He is now part of the Cyber Security Governance team at IHiS, which sets out cybersecurity policies and standards for the public healthcare sector. He recently completed his Healthcare Information Security and Privacy Practitioner certification. This makes him one of only two people in Singapore to possess this important certification.

We will continue to develop health tech talents like Vincent, even if they are not originally trained in health tech skills. It does not matter where you start from. TeSA plays a critical role in this effort. For example, the Professional Conversion Programme, or PCP, enables people with the passion and motivation to move into new fields of work. I am pleased to share that we are supporting more people to enter health tech through the PCP. IHiS is partnering IMDA and Workforce Singapore to bring in around 50 mid-career professionals over the next two years.

Working closely to nurture health tech talents

The second way to strengthen our talent pool is by working closely with our partners such as industry and universities, to train and nurture health tech talents. Such partnerships allow us to leverage one another’s resources and capabilities to make our collective efforts more effective.

I am therefore pleased to welcome MSD as a new partner under the TeSA Company Led Training programme. As a leading pharmaceutical company, MSD is well-placed to boost our health tech workers’ capabilities. MSD will be training more than 90 professionals in high-demand skills such as cybersecurity, data analytics, software development and cloud computing over the next three years.

I am also heartened to know that SMU is launching a new programme in Health Economics and Management. This programme aims to produce graduates who are well-versed in tech skills such as data analytics, which will play an increasing role in the healthcare sector.

Conclusion

Let me conclude my speech by acknowledging the good work done by the TeSA Healthcare Sector Committee in identifying the sector’s future manpower needs. Talents are very crucial for innovation and breakthroughs across all sectors. If you have the passion and the interest, we are here to help you. But I think this can only work if we have the people who have the commitment, resilience and perseverance to go through the iterative process of experimentation and failure. As such, beyond talent development, it is important for us to create the spaces and opportunities that allow us to test out these ideas in real life. This is something that we will be keen to work with our partners, agencies and industry – to find areas in which we can run pilots and test out these solutions in the community. With that, thank you so much, and have pleasant day.