I am pleased to join you today to celebrate the first anniversary of AI Singapore’s formation. It has been an eventful year for AI Singapore and all its partners – companies, educational institutions, government agencies and our collaborators from abroad. I would like to thank all of you for helping AI Singapore get started on a strong footing.

AI Singapore was launched last year as a national programme to boost Singapore’s artificial intelligence capabilities. It is a key initiative under the Services and Digital Economy (SDE) domain of our Research Innovation and Enterprise (RIE) 2020 plan, to enable Singapore and Singaporeans to benefit fully from the growth opportunities in the digital economy.

I would like to congratulate Professor Ho Teck Hua and his team at AI Singapore for their good work and accomplishments over the past year. You have heard from Prof. Ho about AI Singapore’s progress in its three areas of focus – AI industry innovation, research and technology development.

AI Singapore has also helped to strengthen the linkages between industry and research, thereby ensuring that Singapore’s research in AI remains pertinent to industry needs. 100Experiments is an example. It is a programme that not only fosters closer industry-research partnerships, but also innovates practical uses of AI to solve industry problems.

For instance, real-time and unpredictable events such as road accidents and sudden changes in driver or vehicle availability often pose problems to the transport industry. Versafleet is a local transport management software company that is exploring how AI could be used to better understand human behaviour and to develop optimised vehicle routing solutions. Such collaborations with industry are key to value creation by translating our investments in research and outcomes into practical solutions for problems faced by industry.

Key Message 1: Singapore will adopt a practical approach to build AI capabilities in a targeted
fashion

With the increasing digitalisation of the global economy, AI will bring many more growth opportunities. This is why many countries, including the two largest economies the US and China, have significantly ramped up their investments in AI on multiple fronts. Singapore cannot and should not seek to emulate the scale and scope of their approach. Rather, we must identify and develop specific areas of focus that can yield the best returns on our investments, by building on our AI and other capabilities.

In this regard, one of Singapore’s main strengths is our established and recognised R&D base in AI. Based on the Field Weighted Citation Impact of our research, AI Singapore is currently ranked first in the world. In May this year, the influential academic journal “IEEE Intelligent Systems” released its biennial “AI’s 10 to Watch” list – 4 of these young stars are based in Singapore. Yet another strength is our growing and vibrant AI ecosystem. Through the efforts of the Economic Development Board (EDB) and Infocomm Media Development Authority (IMDA), we have seen an increase in the number of both international and local companies with deep AI expertise that are thriving in Singapore. This includes industry giants like Alibaba as well as fast growing companies such as Taiger, Visenze and Data Robot. By leveraging these strengths in AI research and industry capabilities, Singapore is poised for the next moves in this exciting space.

Key Message 2: Singapore will double-down on AI talent development

Our first move will be to invest significantly in the development of AI talent. There is a consensus amongst governments, industry leaders and recruitment firms worldwide that there is a global shortage of AI talent. Singapore needs a deeper bench strength of talent to further grow our AI ecosystem. AI Singapore can and will play a key role in this regard by working closely with its research and industry partners to nurture a strong pipeline of AI talent in Singapore.

Last November, we launched the inaugural AI Apprenticeship Programme, a partnership between AI Singapore and the TechSkills Accelerator (or TeSA), which is a SkillsFuture initiative driven by IMDA and its strategic partners. The 1st batch of AI apprentices was inducted in May this year and they are in the midst of their training. All apprentices in this intensive programme, even those without a background in computer science, go through a combination of in-depth AI courses and on-the-job training on industry projects over the course of nine months.

One example is Eunice Soh, who is working with Surbana Jurong on a project to predict lift breakdowns using AI. Eunice majored in life sciences and does not have a computer science background. While the learning curve has been steep, the apprenticeship programme has helped Eunice and others like her to pursue their career aspirations in AI.

Building on the lessons from the first run, we will scale-up the AI Apprenticeship Programme so that more Singaporeans can obtain the requisite training to become an AI engineer. The 2nd batch of apprentices will commence their programme in November this year.

While some firms require such AI engineers to develop, design and deploy AI solutions at-scale, we also need a differentiated approach to talent development to meet the industry’s varied needs for AI capabilities. For instance, firms also need workers with a foundational understanding in AI and data science, which they can apply for business needs.

[Announcement 1: AI for Industry Programme] I am therefore pleased to announce that AI Singapore will launch AI for Industry, a three-month foundational programme for executives who are technically inclined and keen to learn programming to develop basic AI and data applications. Candidates will undergo a hybrid online and offline programming curriculum with a leading partner in this field, as well as face-to-face workshops.

To make it easier for individuals to pick up AI skills, AI Singapore will partner IMDA to reduce the cost of fees incurred by candidates. The AI for Industry Programme aims to train 2,000 AI users in three years’ time.

[Announcement 2: AI for Everyone Programme] Beyond AI training, it is also important for everyone to develop a basic understanding of what AI and data science are about, and to be able to identify potential use cases at work and in our daily lives. To this end, AI Singapore will launch a free introductory programme, conducted in partnership with Microsoft, Intel and IMDA.

The AI for Everyone Programme aims to get 10,000 individuals exposed to AI and data science in three years’ time. This introductory programme will help more individuals to appreciate the practical uses of AI and go a long way in ensuring that our companies and workers keep up with technological developments and emerging growth opportunities.

Through programmes like AI for Everyone, AI for Industry and the AI Apprenticeship Programme, we will develop a strong pipeline of AI talent for Singapore.

Key Message 3: Singapore will focus on building capabilities that can enable a trusted AI
ecosystem

I have talked about our first move – talent. Our second move relates to trust – building a trusted AI ecosystem. As we all know, the growth of AI would require big data capabilities, and what has been categorised as privacy preservation technologies will be critical for the continued capitalisation of data. Increasingly, we need privacy solutions that can enable data integration across organisational boundaries. We also need more secure solutions, especially new methods of encrypting data on the public cloud.

This is one area which Singapore can target in our efforts to develop deeper AI capabilities. Singapore can build on our strong R&D base to develop a niche in privacy preservation technologies. We had earlier launched a grant call to build capability centres in this area of technological research, and the results will be announced shortly.

With the growing use of AI in our economy and workforce, it also important that we proactively address the ethical concerns that may arise, in order to develop a trusted AI ecosystem which can derive the benefits from the innovations that companies deploy whilst ensuring that we have the confidence of consumer acceptance. Hence, we announced the establishment of the Advisory Council on the Ethical Use of AI and Data led by Senior Counsel V.K. Rajah in June this year.