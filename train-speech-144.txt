Preparing Our Hearts, Heads and Hands for the Digital Future


Good afternoon, friends and fellow colleagues.

We have had an exciting start to the year. In January, the Committee on the Future Economy spelt out how Singapore could seize the opportunities in the digital economy. As Janil, Hong Tat and I elaborated during the Budget and Committee of Supply debates last month, MCI will play a proactive role in making this happen – by helping our companies, especially our SMEs, take advantage of digital technologies to grow. We will also continue to equip our workers with the skills to thrive in the digital future. 

Smart Nation and Digital Government Group

A first-class digital government is of course critical to our success. MCI has been proud to nurture this capability with the former Infocomm Development Authority and now GovTech. With the formation of the Smart Nation Digital Government Group, our colleagues from GovTech and the Government Technology Policy Department will be moving to the Prime Minister’s Office to join this new Group. 

I would like to take this opportunity to thank Jacqueline and her team in GovTech, for their contributions to our Smart Nation and digital government initiatives, such as SingPass, Corppass and the National Trade Platform. I would also like to thank Chee Hau and his GovTech Policy team, for providing oversight to our ICT governance frameworks and ensuring our ICT systems are secure and resilient. We wish them all the best!
 
Digital Readiness

Even as we build a digital economy, and transform ourselves to become a digital government, we will at the same time make sure we nurture a digital society where Singaporeans have access to technology, understand it, and are passionate about using technology in their lives. Technology has the potential to improve and transform our lives, and we want everyone to be able to participate in this digital future and seize all the benefits and opportunities that technology can bring. 

This means a whole range of things: from having affordable access to the internet, to interacting online without fear of being scammed. It might also mean experimenting with new technologies such as 3D printing and augmented reality. You will have encountered some of these technologies earlier as you came into the room.

Today, I want to share our continual efforts to build this digital readiness – by expanding our Hearts, engaging our Heads and creating with our Hands. 

Heart: Improving access to digital technology

First, we need to expand our Hearts and ensure that every Singaporean, especially the less privileged and people with disabilities, are part of our digital journey. In the past two decades, we have helped Singaporeans gain access to basic technology. Through programmes like NEU PC Plus and Home Access, we have provided more than 58,000 low-income families with a basic computing device and internet access. During the COS, Janil announced that we will double the Home Access Programme, to serve another 16,000 families over the next three years.  

Enabling Persons with Disabilities to Access Technology
 
Today, the Enable IT programme helps those with special needs adopt infocomm and assistive technology to support their daily living, education and employment needs. Beneficiaries include 23 year-old Alex, who has moderate intellectual disabilities. Thanks to the Enable IT programme, he was introduced to a Virtual Reality Gaming Solution called Timocco, which renewed his interest in therapy, and developed his skills to lead an independent life. 

We will do more. We want to help people like Alex lead meaningful, independent lives through technology. We will therefore enhance the Enable IT Programme by doubling the grant support to voluntary welfare organisations to $100,000 per project, so that they can acquire assistive technology devices to help more people with disabilities perform tasks independently. This will help another 3,000 beneficiaries over four years, double the number that we are reaching right now. 

To allow people to try out a device before making a purchase, IMDA will set up a library to loan infocomm and assistive technology devices to people with special needs and voluntary welfare organisations. They can even bring the device along for job interviews, to help a potential employer understand their needs better. 

IMDA is also collaborating with tech companies such as Apple and Microsoft to train a pool of people with special needs to be Assistive Technology Ambassadors, to spread the use of built-in assistive technology on their devices and platforms. For example, the VoiceOver in-built screen reader on the iPhone and iPad can describe what is on your screen, which helps people who are visually impaired use them like everyone else. Microsoft Windows and Office also come with built-in accessibility features like magnifier, narrator and speech recognition that will allow persons with disabilities perform tasks with ease. We heard from a visually impaired lady how she can just be like everyone else when it comes to online shopping and ibanking, thanks to such accessibility features. She said, “I can use internet banking to transfer money here and there, and read my bank statement, which is good because when I take my passbook I cannot read my passbook”. 

These Ambassadors will be trained so that they can help people with different disabilities – including those with autism, or physical, visual, and hearing disabilities, use the features on their regular mobile devices to improve their quality of life. 

Head: Enhancing Digital Literacy 

The second H is for “Head”, because it is not enough to have access to technology, but to be useful, we must know how to use it and understand the digital world better. This is critical in an environment where there is an overabundance of information online, where it is hard to differentiate fake news from facts, and where we need to guard against cyber-attacks and online scams. 

Today, different parts of the MCI family run outreach and education programmes. For example, NLB’s S.U.R.E campaign provides tools to check the credibility of information online. CSA’s Cyber Security Awareness Campaign teaches the public to be savvy about the dangers in cyberspace. The Personal Data Protection Commission helps organisations and individuals adopt good data protection practices. IMDA, together with the Media Literacy Council, runs the Better Internet Campaign to raise awareness of safe and responsible online behaviours. 

Holistic Approach to Digital Literacy 

These digital skills are all intertwined, and are skills that every Singaporean needs in the new digital world. Recognising the need to do more to build a digital society, we adopted a design thinking approach to identify areas where we can develop new programmes in a more holistic manner. 

We will reach out to more segments of the population, such as lower skilled and lower wage workers who are most at risk of being left behind in the digital journey. We can do this by designing the information and learning opportunities based on what they are comfortable with. 

We will look at how to equip ‘digital parents’ of today, so that they are not only able to keep up with their children’s technological knowledge, but also guide them and have meaningful conversations with them. For example, during the design thinking process, one middle-aged mother shared with the team that she “wants to be involved in the conversation rather than have my children talking and when they see me they stop because mum don’t understand”.

We will work with various partners to develop these programmes, and will share more details when they are ready. 

Encouraging youths to promote Digital Literacy to the community

However, MCI cannot do this alone. We want to work with you to help others gain digital literacy skills. For a start, I am pleased to announce that the Media Literacy Council will be launching a new Call For Proposals to empower youths to initiate ground-up projects to promote digital literacy to the community. In this programme, MLC together with its partners, Google, Garena, MyRepublic, Facebook and Mediacorp will provide funding, training and incubation facilities for young Singaporeans who want to develop social innovation solutions.

Through this, we want to encourage young Singaporeans to step forward to start their own platform, movement or enterprise, to develop solutions that tackle issues such as cyber-bullying and fake news. We want to support youths as they are the savvy digital natives with great potential and ability to use their hearts for the community.
  
Hands: Developing digital makers

The third H is for Hands. Today, many of us use technology to improve our work processes, our services and help with our everyday lives. To truly reap the benefits of technology, we need to be able to harness it to solve our problems creatively. We want to encourage Singaporeans to have a curious mind and passion to tinker and create.

We have programmes like Code@SG, Lab on Wheels and PlayMaker to promote computational thinking and encourage the Maker mindset. Today, I am pleased to launch the Digital Maker Programme, to nurture a new generation of digital natives to be creators and makers, and seed an enterprising maker culture in Singaporeans. This programme introduces simple and open-ended technology that allows both kids and adults to start off as hobbyists to create and prototype their own inventions. Who knows, one day, we may be nurturing Singapore’s own generation of Steve Jobs. 

Introducing Digital Maker technology to schools 

One technology that allows kids and adults to create and prototype their own inventions is the micro:bit. The micro:bit is a pocket-sized, codeable computer with motion detection and Bluetooth technology. It is already being used in schools in the UK, in subjects as different as Computer Science and Geography. Over the next two years, IMDA will work with MOE to introduce digital making to schools by providing primary and secondary schools with micro:bits to enable fun and hands-on learning. 

Students in participating schools will be encouraged to play with the micro:bits and experiment with them to design useful contraptions that can solve real-world problems. For example, some students from Xinmin Secondary School used the micro:bit as a locator tool to help them find their belongings, by detecting the presence of another micro:bit tagged to their items. Microsoft Singapore will be our partner to provide training for teachers to incorporate digital making into their school programmes.  

Build Communities of Digital Makers

IMDA will also work with the People’s Association, the Science Centre Singapore and Centre for Fathering to introduce digital making to the community by providing micro:bits to participants at Digital Maker workshops. We will also work with self-help groups to extend such workshops to low-income households. For example, we introduced digital making to 200 students at Mendaki’s Future Ready Seminar last October, where they made digital name badges using the micro:bit.

The Digital Maker workshop does not only give people a chance to tinker with technology, but also brings families and communities together to build bonds. In IMDA’s early trials with Tanjong Pagar CC, residents who attended the parent-child workshop had fun-filled sessions making digital greeting cards that can play their family’s favourite tunes.  Toa Payoh East CC residents also made use of the micro:bit to create their very own automated water system for their community garden. Some of these participants are here today. You can find out more at the booths outside later.  

Spur local companies to develop and market products

To encourage local companies to develop maker-centric products, seed funding will be provided to local companies with the potential to commercialise. We are already seeing some promising “made-in-Singapore” products coming up.  

For example, local start-up Tinkertanker has developed a breakout board that easily connects to a wide variety of sensors and displays for more complex creations, without having to deal with messy wiring and soldering.  This is especially useful for the classroom as students can focus on their digital creations rather than having to resolve bothersome issues like wiring. The breakout board was featured at an education technology conference this January in London and is being manufactured for sale in the UK.  

Our local SME, Home-Fix, is also developing a Digital Making Starter Kit that allows the user to build 15 different projects with the micro:bits.  You can view these prototypes at the booths outside later as well. 

Set up of digital readiness programme office 

Many countries around the world, such as the UK, US, Hong Kong and Australia, have already developed national strategies to ensure that all segments of their population are digitally ready to reap the full benefits of the digital infrastructure available. As you would have learned by now, many public, private and voluntary organisations here are already embarking on their own digital readiness initiatives or with different partners. 

To ensure greater alignment and synergy among all these various initiatives, MCI will set up a digital readiness programme office to drive a whole-of-nation strategy to build a digital society, and coordinate the efforts of the public, people and private sectors. We envision this team to function as the centre of an extensive network of organisations, and will study how digital readiness is developed internationally, identify existing gaps in our current efforts, and recommend a long-term strategy and manifesto to build a digital society. We will share more details when our plans are ready. 

Digital readiness is a national effort

Digital readiness is a national effort. We want to help everyone to participate together in this digital future, not just to learn, but to seize all the benefits and opportunities that technology can bring. 

No effort is too small or too simple, and everyone can play a role in this journey. It can be teaching your grandparents how to use a smart phone, sharing with your children about safe online behaviours, or fiddling around with a micro:bit. The government will lead efforts and provide a supportive environment for co-creation, but we need all sectors of society - community groups, businesses, schools and VWOs – to work together with us.