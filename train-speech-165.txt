Building an inclusive society

We held a series of SGFuture engagements earlier this year, kick-starting a national conversation to reflect and co-create the kind of society that we want to become. We envisioned a fair and inclusive society, where every individual has the opportunity to excel; where everyone can lead meaningful lives; and where his or her contributions to society are valued.  

Being an inclusive and a truly Smart Nation means that we not only have to stay ahead of the curve, but also to bridge the gaps to ensure that no one is left behind. Today’s forum is a part of this ongoing conversation to explore how we can draw on information and assistive technology (IT/AT), to shape a society that embraces and empowers persons with disabilities. 

Improving the quality of life for persons with disabilities with IT/AT

Assistive technology is not a new concept. Many of the devices that we know today, from wheelchairs to prosthetics, are refined from past innovations. Over the years, the range of assistive technology has expanded to include IT-enabled devices such as audio-to-text mobile apps to assist people with hearing impairment. The Singapore University of Technology and Design and the Massachusetts Institute of Technology are even co-designing a finger reader that helps the visually-impaired read with a swipe of their fingers. The possibilities of IT/AT are indeed endless. 

We strongly believe that such technology can enable and empower persons with disabilities in their daily lives. This is why Tech Able was set up here at the Enabling Village to encourage persons with disabilities to adopt assistive technology. 

Since its launch in October 2015, more than 4000 people have visited Tech Able to learn more about assistive technology. To date, its Assessment and Referral Service has helped over 150 persons with disabilities make informed choices on devices best suited to their needs. 

An individual who has benefited from Tech Able is Ang Chin Hao, who was born with a rare genetic eye disorder that causes vision loss. He uses an “E-Bot Pro Magnifier” which his school, the Nanyang Technological University, had purchased upon Tech Able’s recommendation, for him to use in his studies. This device helps to magnify content shown on the screens, which makes it easier for him to read and complete his assignments. 

Supporting and caring for persons with disabilities through IT/AT initiatives

In our community, we have role models like Chia Hong Sen, a graduate from Temasek Polytechnic. Hong Sen is with us today. Although he is visually-impaired, he exhibited much verve and tenacity in his studies: learning to read, type and even note music scores in Braille. He pursued his interest in IT in polytechnic, making use of IT/AT solutions, such as a special software that emits sound cues to help him hear and write codes for his programming classes. Along the way, Hong Sen had also received great support from friends and teachers who cared and looked out for him. Now, he is paying it forward. He teaches the visually impaired at his alma mater, the Singapore School for the Visually Handicapped, how to use their smartphones.  

Stories like these remind us of the important role that each of us undertake in supporting and caring for persons with disabilities. 

As policy makers, our role is to provide the infrastructure and policies to create a more inclusive environment. This means finding ways in which technology can help to improve our processes and services; making technology accessible to those who need it; and ensuring that persons with disabilities can afford devices that suit their needs.  

The Infocomm Development Authority (IDA) is making IT more accessible to persons with disabilities from low-income families through its Digital Inclusion programme. I am happy to announce that IDA will be enhancing its NEU PC Plus Programme in two ways. First, we will increase the household income cap from $3,000 to $3,400, so that more individuals will be able to benefit from the scheme. Second, we will streamline the application process, so that special education students already on financial assistance will be able to receive up to 75% subsidies more easily. We hope that these enhancements can better support our students in their educational journeys. 

Today, IDA is also unveiling its first assistive tech bus under the Lab on Wheels programme that connects different groups of people to technology. This bus will be wheelchair-accessible, outfitted with IT/AT gadgets, and offer workshops with customised curriculum that cater to persons with special needs. In addition to community centres and shopping malls, the bus will also travel to special education schools run by Voluntary Welfare Organisations, so that our students in these schools will have the opportunity to tinker with, learn and benefit from IT/AT. The bus is parked here today, and I encourage all of you to try out the gadgets on board.

Connecting persons with disabilities to the community  

Voluntary Welfare Organisations can also play a part by helping their clients make full use of enabling technologies, and integrate into the community. I am glad that organisations such as SPD[1] and the Movement for the Intellectually Disabled of Singapore (MINDS) are progressive in their use and championing of IT/AT devices. With the support from IDA’s IT/AT Adoptive Grant, both of them are implementing virtual reality and sensory technology, to help their clients develop cognitive and motor skills that enhance their functional living and social interaction abilities. Several of these tools are available at the exhibition area, so do grab this opportunity to experiment with them.

Clients at the Blue Cross Thong Kheng Home (BCTKH) are also actively engaging with IT/AT tools, such as a Smart Interactive Whiteboard and tablets with apps that were purchased through the Grant. These tools have helped to extend their attention spans during activities where they learn to write, draw, and even complete crossword puzzles. 

I was heartened to read the news that more employers in the private and public sectors are keen to hire persons with disabilities. In fact, employers such as DataPost here in the Enabling Village have employed persons with disabilities, and enabled them to build greater self-esteem and confidence. I hope to see even more employers give them the opportunity to achieve their potential and contribute to society. 

Everyone has a part to play in building an inclusive society. With your strong support and partnership, we can achieve even more together. I hope this forum will inspire you to innovate and exchange ideas on how we can continue to improve the quality of life for persons with disabilities, support and connect them to the wider community. I wish all of you a fruitful forum. Thank you.