Developing a vibrant and innovative digital economy

Singapore is proud to host Innovfest Unbound. Like the many companies and entrepreneurs that are featured this week, Singapore has had to think and operate like a start-up to survive as a small open city-state.  We have had to be nimble, act quickly and constantly seek new markets to grow and improve our peoples’ lives.  In our next phase of development, we aim to be one of the world’s leading Smart Nations.  To realise our vision of becoming a Smart Nation, we are building strong capabilities tap on opportunities in the Digital Economy.

We are making steady progress in this effort.  We launched the InfoComm and Media (ICM) Industry Transformation Map (ITM) last year and the Digital Economy (DE) Framework for Action two weeks ago at the Singapore Digital (SG:D) Industry Day.  We also launched the Digital Readiness (DR) Blueprint 3 days ago to ensure that no citizen is left behind in this digital transition.  At Budget 2018, we announced that a further $145 million will be invested to scale up the Tech Skills Accelerator (TeSA), and that we are on track to create 20,000 training places by 2020.

Today, I would like to share three more ways in which we plan to grow the Digital Economy. They are:

a.	Leveraging frontier technologies like the Artificial Intelligence;
b.	Harnessing data; and 
c.	Growing our international networks.


Leveraging frontier technologies and exploring new growth areas, especially in the field of Artificial Intelligence

First, we must leverage frontier technologies such as Artificial Intelligence (AI).  Its immense potential, especially when combined with strong data, is well-recognised and that is why we have launched industry programmes to support the development and adoption of AI solutions.  It is a journey that private enterprises and consumers must walk together with the Government.  And we must also be plugged into the global flow of ideas, talent and opportunities. 

As multinational technology companies choose Singapore as one of their R&D locations, our AI Apprenticeship Programme will provide opportunities for local talent to work with the global best.  Started last November, it is off to a good start with the first batch of 13 apprentices commencing training in May, and we are on target to train 200 apprentices over 3 years under the TeSA scheme.

Our Accreditation@SG programme will continue to identify and support ambitious local enterprises that aim to reach into global markets.  The programme has benefited local AI solutions providers like Visenze, which today provides its computer visioning and deep learning AI engine to the likes of H&M, Rakuten and Uniqlo.

We established AI Singapore in May 2017 with S$150 million funding to catalyse, and boost Singapore’s AI capabilities.  AI Singapore brings together all Singapore-based research institutions and the vibrant ecosystem of AI start-ups and companies developing AI products, to grow the knowledge, create the tools and develop the talent to power Singapore’s AI efforts. 

AI Singapore’s 100 Experiments (100E) programme matches companies which are keen to use AI to address their problem statements, with the local researchers who are interested to tackle those problems.  The 100E programme has received strong industry interest, and has engaged over 170 companies from diverse sectors including healthcare, finance and media.  For example, AI Singapore is working with local start-up Kronikare to develop a thermal imaging solution incorporating AI for better assessment and care of chronic wound patients.  This new solution is currently being trialled at St Andrew’s Community Hospital (SACH).

IMDA will continue to work with AI Singapore to develop the AI ecosystem to strengthen Singapore’s ICM core, and position us well on the world’s AI map.

Harnessing the value of data

Second, we must harness the value of data as the building block of the Digital Economy.  AI requires data to be effective.  Hence, we want to encourage data sharing among businesses to solve common problems, drive innovation and unlock the value of complex datasets.  This includes giving enterprises access to datasets for machine learning applications. 

The Government is doing its part by enhancing data.gov.sg to make more datasets and real-time data from our public agencies available to businesses and citizens.  IMDA’s Data Innovation Programme Office also encourages partnerships among businesses to share data with one another responsibly to solve common business problems.  For instance, IMDA will partner local technology solutions provider, DEX, to tackle the question of how to generate trusted data frameworks for data sharing.  This is being done in conjunction with major companies like Aviva, Unilever, and Roche, to work on problem statements in areas such as wellness, healthcare and consumer products.

As data sharing models develop, IMDA will continue to actively engage the industry to develop best practices, guidelines, and case studies for good data sharing practices and help inform policy as we develop a forward-looking regulatory environment. 

To build a progressive regulatory framework for Singapore

For companies to thrive in a Digital Economy, it is essential to establish a conducive and consistent regulatory environment that encourages innovation.

In this context, the Personal Data Protection Act (PDPA) regime seeks to meet the legitimate needs of organisations to collect, use and share personal data to come up with better and more innovative goods and services, with increased accountability by organisations to safeguard consumer interest and trust.

We are reviewing the PDPA to see how the framework could be enhanced for enterprises to collect and use personal data.  One model could be a potential notification of purpose followed by an opt-out period, as well as to provide for legitimate interests as a basis for the collection, use and disclosure of personal data where benefits to the public are significant.  In such cases, there must be a balance by ensuring that robust accountability measures are put in place to safeguard the interests of individuals.

Discussion Paper on AI and Data Governance Framework

To encourage the adoption of AI, we will adopt the same progressive regulatory stance as we have with other innovative technologies. Innovative technologies bring economic and societal benefits, as well as attendant ethical issues.  Thus, good regulation is needed to enable innovation by building public trust.

In order to raise awareness of the benefits and understanding of the challenges of AI, we are engaging key stakeholders, including industry, consumers and academia, to collaboratively shape the Government’s plans for the AI ecosystem.  One example is the recent effort by PDPC to develop a Discussion Paper on AI and Personal Data. 

The paper will form a baseline for discussion, and describes the key components of responsible development and adoption of AI, including governance, consumer relationship management, and the decision-making and risk assessment framework.  Two key principles that have been put forth are that (i) decisions made by or with the assistance of AI should be explainable, transparent and fair, and (ii) AI systems, robots and decisions made using AI, should be human-centric.  This is intended as a living and evolving document to provide a common language for both public and private sector organisations that deploy or wish to deploy AI, and to support collaborative discussions on how to use AI responsibly even as technology progresses. 

The Monetary Authority of Singapore (MAS) has been an active collaborator in the conceptualisation of the discussion paper.  In partnership with key stakeholders, MAS will use the paper to help shape its ongoing discussions on the use of AI and data analytics in the financial industry. 

Advisory Council on the Ethical Use of AI and Data

I am also pleased to announce the establishment of an Advisory Council on the Ethical Use of AI and Data.  The Council will comprise representatives from technology companies, users of AI, and those who can provide consumers’ and societal perspectives.  The Advisory Council will work closely with key stakeholder groups to lead discussions and provide guidance to the Government on the responsible development and deployment of AI.  It will:

a.	Engage relevant stakeholders such as ethics boards of commercial enterprises on ethical and related issues arising from private sector use of AI and data, as well as consumer advocates on consumer expectations of such use; and
b.	Engage the private capital community to increase awareness of the need to incorporate ethics considerations in their decisions to invest in businesses which develop or adopt AI; 

The Advisory Council will assist the Government to develop ethics standards and reference governance frameworks, issue advisory guidelines, practical guidance and codes of practice for voluntary adoption by businesses.  Former Attorney-General, Mr V.K. Rajah SC, has kindly accepted our invitation to serve as the Council’s inaugural Chairman.  We look forward to his leadership and the contributions from the Advisory Council.  (More details on the Council will be given in due course.) 

Research Programme on Governance of AI and Data Use

To support the work of the Advisory Council, we will also be setting up a five-year Research Programme on the Governance of AI and Data Use.  This programme seeks to advance discourse in ethical, legal, policy and governance issues arising from AI and data use.  I am pleased to announce that this Research Programme has been awarded to the Singapore Management University (SMU).  It will enable Singapore to drive thought leadership on these issues and serve as a centre for knowledge exchange with international experts.  It will adopt an international perspective and track international developments in these specific research areas. 

Through the Research Programme, SMU will be organising engagement forums with stakeholders to generate and share knowledge, and to bring greater clarity to policy and regulatory issues and their impact.  SMU will also take the lead to publish research papers to contribute to the wider international AI and data research communities. 

Growing our international networks

Finally, to grow our digital economy, we must enhance our international networks, especially in an increasingly peer-to-peer (P2P) and networked world.

This is why we are committed to hosting international conferences like Innovfest Unbound (IU).  Over 12,000 participants from more than 100 countries are gathered for the event, with more than 350 tech innovations and start-up exhibits on display.  IU brings together a global community of start-ups, investors, institutes of higher learning, government agencies and corporates to showcase the latest in innovation and technology, to share knowledge and explore business collaboration opportunities. For instance, PIER71, an initiative between the Maritime and Port Authority of Singapore and the NUS Enterprise, seeks to grow a vibrant maritime entrepreneurial ecosystem that helps tech start-ups gain access to international and local companies, venture capitalists and mentors.  In April, PIER71 signed a Memorandum of Understanding (MOU) with PortXL, a maritime-only accelerator for start-ups with offices in Rotterdam and Singapore, to increase the diversity of our maritime innovation ecosystem.  Under the MOU, both parties will seek to encourage start-ups globally to participate in pilot projects and provide access to the maritime industry in Singapore and in Rotterdam.  Such events are key to showcasing Singapore and our companies to the world.

This year, we have folded IU into the larger Smart Nation Innovations Week, together with pillar events like Digital Government Exchange (DGX), Tech Saturday (Upsized!) and Straits Digital Exchange (SDX).  We hope this will help to expand Singapore’s international networks and circle of friends from around the world.  The discussions and sharing sessions over the next two days are excellent opportunities for our participants and start-ups to exchange innovative ideas, strengthen partnerships, and share experiences.  I look forward to seeing many new possibilities ignited by the discussions at this event. 