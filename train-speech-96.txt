Good morning. It is my pleasure to join you today to commemorate Total Defence Day 2019.

Fort Canning Hill, where we stand today, holds an indelible place in our national memory.  The Battle Box, which is just around the corner, was where Lt-General Percival made the fateful decision to surrender Singapore to the Japanese on 15 February, 1942.  

Total Defence has kept Singapore strong 

Many of us here today did not have to endure the dark years of the Japanese Occupation; but we would have heard stories from our parents or grandparents.  Hunger was a daily experience, and people lived in constant fear of being arbitrarily captured and tortured by the Kempeitai.  We must never let anything like that happen again in Singapore.

It is therefore apt that we are joined here today by the recruits from the Commando Training Institute.  It is especially significant that you are here today to formally receive your weapon, as you make the symbolic transformation from civilian to soldier, carrying arms to defend our country.  We thank you for your service and commitment to defend our nation.

The trying years of the war also bore witness to the resilience of the human spirit – as our people rallied together, supported one another, and overcame adversity.  For example, as there was a severe shortage of rice, sugar, and salt, people grew their own food, such as tapioca and sweet potatoes.  And even though they barely had enough to feed themselves and their families, some made food for the even less fortunate, such as the prisoners-of-war who were left on the streets after being tortured.

These stark memories are a vivid reminder of the importance of building a strong SAF and being prepared for emergencies. They also underscore that defending Singapore includes building a strong economy, living harmoniously with people of all backgrounds, and forging a resilient society with a strong shared Singapore identity. These imperatives are encapsulated in the five pillars of Total Defence – military, civil, economic, social, and psychological.

Total Defence recognises and emphasises that each and every citizen has a part to play in defending our nation and our way of life.  Our collective commitment to Total Defence has kept our nation strong since its launch in 1984. 

Today, we face new threats from the digital domain

However, today, the digital domain poses a new type of threat to our safety and security.  We have always had to deal with conventional threats and terrorism in the real world.  Today, we are confronted by the scourge of online scams, fake news, and cyberattacks in the digital world.  We are all well aware of the recent cyberattack on SingHealth, our largest cluster of healthcare institutions. This was not the first instance when we were the target of a cyberattack, and it will certainly not be the last.

Malicious cyberattacks can and do debilitate entire systems, disrupt the economy and daily lives, and even lead to injury and death. The December 2015 cyberattack on Ukraine’s power grid left 230,000 people without electricity for up to 6 hours, in the middle of a winter night.  In 2017, the WannaCry ransomware attack crippled the operations of about one-third of public hospitals in the UK and caused over 19,000 appointments to be cancelled.

What happened in Ukraine and the UK could just as easily happen in Singapore.  We are an open and highly connected city state.  The very connectivity that we rely on for economic growth and efficient public services, also leaves us vulnerable to threats from the digital domain.  

Hybrid operations involving hostile information campaigns and the spread of deliberate online falsehoods are an especially pernicious threat.  They can foment distrust between ethnic and religious communities, weaken social cohesion, and trigger violence.  Deliberate online falsehoods pose a particularly serious threat to Singapore given our high Internet penetration and multi-ethnic, multi-religious society.  

In 2018, the widespread circulation of false rumours on social media, coupled with an unfortunate miscommunication at a restaurant, led to violence between Buddhists and Muslims in Sri Lanka. Here in Singapore, false claims about the disruption of a Thaipusam procession in January 2018 by a police officer and a member of the Hindu Endowments Board were spread widely online and could potentially have caused problems along racial or religious lines.

Also, digital threats are not as easily discerned as conventional threats. Hackers can enter our systems with us becoming aware only when something dire occurs.  Online scams have been rising in variety and sophistication as scammers become more skilled at creating believable URLs and customised phishing messages. For example, since September 2018, over 90 victims have been deceived into providing DBS Internet banking details on fraudulent websites. And it will only get more difficult to distinguish truth from falsehood as artificial intelligence exacerbates the spread and seamlessness with which deepfake images and videos can be created.

We must prepare ourselves by nurturing the instincts and acquiring the skills that are necessary to defend ourselves against these new threats of a digital age.

Digital Defence as a sixth pillar of Total Defence

Therefore, today, in conjunction with the commemoration of Total Defence Day, I want to announce the addition of a sixth pillar to Total Defence.  That sixth pillar is Digital Defence.

Our vision is for Singapore to be a vibrant Smart Nation.  To that end, we are creating opportunities in the Digital Economy for our businesses and our people.  We are transforming into a Digital Government to deliver better services to citizens and companies.  We are also strengthening the Digital Readiness of Singaporeans, so that all of us can share in the benefits and opportunities afforded by technology.  Whether we succeed or fail in this important endeavour will depend critically on whether we also have a strong Digital Defence.  

Let us watch a short video that explains what Digital Defence is about. 

Digital Defence in action

How do we put Digital Defence into action? Each of us can play a part by being secure, being alert, and being responsible online.

First, we can be secure by practising good cybersecurity habits to safeguard data and devices.

For individuals, the Cyber Security Agency of Singapore (CSA) offers four “Cyber Tips 4 You” – using strong passwords and 2-factor authentication, installing anti-virus software, updating software as soon as possible, and looking out for signs of phishing.

To spread awareness, CSA’s Cyber Savvy Machine, which allows people to take a short cybersecurity quiz and win a prize, is roving islandwide to one library every month this year. 

I am also happy to share that the Infocomm Media Development Authority (IMDA) has developed a new cybersecurity theme for its Lab on Wheels programme. This enables participants to take part in a series of activities to experience and understand their vulnerability to cyberattacks, identity thefts, and internet scams.  In fact, the bus is here today, and I invite all of you to explore it later.

For organisations, IMDA’s SMEs Go Digital programme offers advice and subsidies for pre-approved cybersecurity solutions as part of the Start Digital Pack to help SMEs protect their systems as they digitalise their businesses.  I am happy that local companies such as SgCarMart and the Singapore College of Insurance have already adopted solutions by InsiderSecurity, a homegrown cybersecurity company founded by Singaporean cyber warfare experts and accredited by IMDA’s Accreditation@SGD programme.  SMEs can approach the nearest SME Centre for basic advice, or the SME Digital Tech Hub for more advanced needs.    

Secondly, we can be alert by being vigilant against fake news and disinformation.

A useful guide is the National Library Board’s (NLB’s) Source. Understand. Research. Evaluate., or S.U.R.E., approach.  This four-step approach calls on readers, when confronted with a piece of information that looks suspicious, to consider whether the Source is trustworthy, followed by Understanding what one is reading and distinguishing facts from opinions.  The next two steps are then to Research by cross-checking the information with other reliable sources, and to Evaluate the veracity of the information by exercising sound judgment.

A discerning citizenry is our first and most important line of defence against deliberate online falsehoods.  That is why the Government is significantly expanding our efforts in this regard.  For example, NLB has enhanced its S.U.R.E. programme to cater to the needs of different segments of the population, and is working with various partners to raise awareness.  The NTUC LearningHub incorporates S.U.R.E tips in its training programmes, such as selected runs of the SkillsFuture for Digital Workplace (SFDW) programme which equips Singaporeans with digital skills for the future economy.  

Third, we can be responsible by considering the impact of our actions on the broader community when we produce or amplify information online.

Determining whether a piece of information is true can be time-consuming and challenging, especially as fake news becomes increasingly sophisticated and pervasive.  As a responsible member of the online community, what we can minimally do is not to propagate or amplify fake news.  If we doubt the truth of a piece of information, we should do the right thing and choose to not post or forward it. 

Digital Defence as a whole-of-nation effort

Digital Defence is a whole-of-nation effort.  While there are various Government efforts as I have outlined, there are also various commendable community initiatives that promote secure, alert, and responsible behaviour online.

In 2017, a team of student volunteers from Nanyang Polytechnic launched an interactive game to educate the public about good cyber hygiene practices.  The current team leader is Joseph Teo, and he keenly understands the need for greater public awareness of basic cybersecurity habits. He became especially aware of this because his father fell victim to both a phishing attack and a malware attack.  This made Joseph realise how easily anyone could become a victim of cyber threats, and it spurred him and his teammates to share this game with others.

The Media Literacy Council (MLC) comprises members from the people, private, and public sectors, and spearheads public education on media literacy and cyber wellness.  The MLC’s 2018 Better Internet Campaign comprised five mini campaigns tackling issues such as cyber-bullying, cyber safety, and online falsehoods.  The MLC’s 2019 Better Internet Campaign will further roll out various fact-checking resources to the community. 

Call on every Singaporean to play a part in this national effort 

In the Internet world, we are all equal – with the same privileges of access and the same responsibility to safeguard our digital commons.  A central authority serving as the sole gatekeeper is neither viable nor is it keeping in spirit with the Internet, which is a great leveller.  Collectively, as netizens, we are all the stakeholders, beneficiaries and guardians of this shared digital realm. 

Whether a hacker can break into our critical systems depends on the weakest link; so we must strengthen our individual cyber defences.  Whether a malicious perpetrator can fan the flames of distrust depends on our digital awareness and resilience; so we must stand together, speak up and call out against fake news.  Ultimately, the individual citizen is on the front line of Digital Defence, and we each have a critical role to play by being secure, alert, and responsible online.

Conclusion

Over the past 35 years, Total Defence has been invaluable as the rallying call to each and every Singaporean to do our part in defending our nation against the challenges that threaten our independence, well-being, and way of life.

With new threats looming, it is timely and apt that we augment Total Defence for a digital era by including Digital Defence.  We must strengthen our collective resolve and come together as a nation to strengthen our Digital Defence, to keep Singapore safe and secure, in both the real and virtual worlds. 