Singapore as a Smart Nation

The theme of this year’s forum is “Smart Cities and Smart Nations”. This is very pertinent because this is what many cities all over the world are aspiring to become. Being a smart city is not for technology’s sake, but because it is the best way to improve people’s lives in a rapidly changing world with technology forming an increasing part of our lives.

Singapore is no different. We have set our ambitions high to be the world’s first Smart Nation. Back in 2014, we officially launched the Smart Nation initiative. Last year, we also formed the Smart Nation and Digital Government Group Office within the Government to drive our Smart Nation efforts. We are confident that many of the exciting projects that we are developing and implementing will lead to smarter products and solutions that improve our lives, and support our work and play.

IoT plays a critical role in helping us realise our Smart Nation vision. IoT is not just about being connected to multiple devices, but connecting them to a living, breathing, dynamic network. IoT is able to help us deliver better living experiences, enhanced products for consumers, improved work processes, more efficient public services, and many more. In short, IoT is a key enabler in improving our lives.

In building a Smart Nation, we have been putting several things in place to improve lives at a national, industry and individual level.

National Level

First, a Smart Nation needs to be supported by a robust infocomm media industry. Thus, we have developed a holistic strategy to strengthen this industry.

In November last year, we launched the Infocomm Media Industry Transformation Map (or ICM ITM) in Singapore, which details ongoing efforts amongst the government, industry and research community to transform and catalyse the growth of the ICM sector. We expect to grow the Infocomm Media industry by a CAGR of around 6%, which is almost twice as fast as the overall economy.

In this plan, Singapore will be focusing our efforts on four key frontier technologies, and one of them is the Internet of Things or IoT, the theme of this forum. The other three are Artificial Intelligence and Data Analytics, Immersive Media, and Cybersecurity. I will elaborate on IoT.

We believe the advent of more advanced IoT technologies will give rise to many new possibilities to transform businesses and provide a platform for enhanced sophisticated use of data. In fact, McKinsey puts the value of the transformative impact of IoT at about 11% of the world economy by Year 2025, at the higher end of their estimates – that is around US$11 trillion.

We will continue to ramp up our efforts in growing the ICM industry, and encourage the adoption of technology by individuals and companies. In the coming months, we look forward to share more about our plans for the IoT space, as well as other technology focus areas.

In the meanwhile, we have been embarking on several strategic national projects under our Smart Nation drive. One of these projects is the Smart Nation Sensor Platform, which aims to accelerate the deployment of sensors and other IoT devices that will make our city more liveable and secure. These data would serve a variety of purposes in the areas of security, transport or municipal services. For example, under the PolCam initiative, the Singapore Police Force has installed some police cameras in high traffic public areas to further enhance the safety and security of our neighbourhoods and public spaces.

With the advent of IoT developments and as people and devices become more connected, issues of safeguarding personal data and managing cyber threats become increasingly important to us as well.

IoT devices can collect a lot of information about their users. For example, wearables can track your steps, heart rate, sleep patterns, etc. However, this means an increasing amount of personal data is being collected, including sensitive information such as health data. Measures will need to be taken to protect this large and growing volume of personal data.

The Personal Data Protection Commission (or PDPC) will continue to administer and enforce the Personal Data Protection Act (or PDPA) in order to safeguard consumers’ data, and is in the process of reviewing the PDPA to reflect our ambitions of becoming a trusted global hub for innovative uses of data.

In addition to protecting consumers’ data, our data protection efforts also seek to ensure that businesses can use the personal data collected by IoT devices for legitimate innovative business purposes. On the cybersecurity front, the government is also taking proactive measures to study threats and mitigate risks.

Industry Level

At the industry level, we can maximise the use of IoT sensors to gain meaningful insights and increase productivity, such as through Intelligent Data Centres.

In a bid to create more intelligent data centres in Singapore, IMDA is working with the National Supercomputing Centre of Singapore (NSCC) to optimise the data centre’s energy consumption through the use of IoT, Big Data and Machine Learning technologies. Through the proof-of-concept projects, I am glad to share that we have developed intelligent sensing technology to profile the health of the data centre’s operations, detect any anomalies and monitor its energy consumption. This can help to reduce the cost of the data centre’s operations and enhance its workforce productivity.

IoT can also be used to boost efficiency in the manufacturing industry. For example, German semiconductor Infineon Technologies is investing S$105 million over the next five years to develop a ‘smart factory’ at its Singapore manufacturing plant as part of its Smart Enterprise Program. With this initiative, manual tasks that were previously error-prone can be automated, and workers can be channelled to higher value added activities. New technologies will be employed to enhance Infineon’s productivity, such as using automated decision making for scheduling and dispatching, using robotic arms to manage testing boards, as well as using automated guided vehicles to transport chips and materials. With all these, Infineon’s output is expected to increase by four times over these five years.

Many other companies have also embarked on similar initiatives to optimise their processes and are seeing a reduction in capital expenditure, a shortening of lead time and an overall boost in productivity.

Individual Level

Technology also acts as a powerful tool in improving the lives of Singaporeans. I am happy to share a few exciting initiatives that we have embarked on.

The number of elderly citizens in Singapore is expected to triple to 900,000 by 2030. With this growing number, we are enhancing healthcare to deliver holistic healthcare and eldercare, and aid active ageing. The use of technology is going a long way in helping our seniors age gracefully, while providing caregivers a peace of mind while they are away from home.

We are currently test-bedding Smart Elderly Alert Systems in our newer residential estates. Sensors in the flat can monitor the movements of the elderly, and alert caregivers to irregularities, such as when no movements are detected for a prolonged period of time. Wireless panic buttons can also allow the elderly to alert their loved ones quickly in times of need.

In time, we will also launch a new telehealth initiative. The Vital Signs Monitoring system will enable the remote monitoring of vital signs such as the blood glucose level, blood pressure or weight of patients with conditions such as diabetes, hypertension, or heart disease. Patients will be able to receive more timely interventions to manage their health conditions without having to visit the hospital. This system will enable patients to receive more regular care in convenient locations of their choice, and improve the productivity of our healthcare professionals and providers.

The use of technology is also helping us to create a reliable, convenient and cost effective public transport system that can quickly adapt to meet public demand.

We are currently exploring rolling out autonomous public buses in some of our newer housing estates, to supplement manned buses and to enhance first and last mile connectivity for commuters.

These are examples that demonstrate how IoT can impact our everyday lives. We look forward to more of such IoT innovations in time to come. This brings me to my next point on Innovation. Having a spirit of innovation is crucial in exploring new frontiers of technology – it is about finding new ways to do things and constantly asking ourselves, “How we do things in a better way?”

Innovation

First, we should never stop innovating to find new ways of harnessing IoT technologies and encouraging the adoption of IoT in the present and future.

We have been working towards developing cheaper and longer-lasting sensors across a wide variety of industries, from logistics to energy and water usage. Such sensors are driven by cutting edge Low Power Wide Area Network (or LPWAN) technologies such as NarrowBand IoT (or NB-IOT), and LTE Cat-M1. This would mean that sensors can be deployed to places without power access, and costs of maintaining these sensors would be reduced. Overall, this would help to increase the reach of IoT devices at lower costs.

Second, innovation comes hand in hand with a vibrant and well-connected community of industries, companies, researchers and academics working together in partnership. Besides developing a vibrant cluster of companies, Singapore has built peaks of research excellence as well as many synergistic public-private partnerships.

The Agency for Science, Technology & Research or A*STAR’s Industrial IoT Research Programme is one such example to demonstrate Singapore’s commitment to build research excellence and capabilities in IoT. It harnesses multidisciplinary capabilities from A*STAR research institutes as well our Institutes of Higher Learning, such as NUS, NTU and SUTD, to break new ground on cognitive and secure industrial IoT systems. These research outcomes are then piloted at A*STAR’s Singapore Institute of Manufacturing Technology (SIMTech) and Advanced Remanufacturing and Technology Centre, allowing SMEs to test new technologies before their adoption.

To further develop Singapore’s next generation manufacturing sector, A*STAR has established an Industrial IoT consortium with 13 companies, as well as a Smart Manufacturing Joint Lab with Rolls Royce and Singapore Aero Engineering.

Third, for us to maintain our innovative edge, we need to think beyond the shores of Singapore. We strongly believe in the importance of being connected to a global network of international partners – scientists, academics, experts in this field and industry players, like yourselves.

In hosting the World Forum on Internet of Things, we see a huge opportunity to be connected to this community from all over the world, and learn from the many exciting exchanges during this forum. We invite you to experience Singapore for yourselves, and share with us your ideas on how IoT can be applied to make Singapore the world’s first Smart Nation.

I will like to conclude by reiterating the fact that the ultimate goal for Singapore’s Smart Nation vision is to improve the lives of everyone, and I believe that the smart and innovative use of technology such as IoT will take us a step closer to this goal.
