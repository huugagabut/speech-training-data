Creating quality local content

First, on content, which is the heart of the media sector. Compelling stories are what attract audiences. 

Our local media companies have done well in this regard, creating quality local content appreciated by viewers around the world.

For example, Netflix commissioned its first Chinese-language series, Bardo, by teaming up with IFA Media, a local production company, and Taiwan-based director Sam Quah. I am glad that a Singapore-based media company is involved in bringing its stories to a global platform.

IMDA will continue to support our local media companies and talent to create quality content as part of its plans to develop Singapore’s media sector. We will announce more details later in the year. 

One important area is to strengthen our partnerships with leading international media companies.

For example, the collaboration with HBO Asia will open opportunities for Singaporeans to take on key production roles in HBO’s upcoming productions. One of these is Folklore, a horror anthology created by local filmmaker Eric Khoo. Another is Grisse, a period drama set in 19th-century Java. Both series will premiere in the second half of 2018. 

For Public Service Broadcasting or (PSB), locally produced PSB programmes remain an important avenue to tell our Singapore stories and present news and entertainment from a Singaporean perspective. Mediacorp will experiment with new content concepts and productions techniques through the Content Development Fund. 

The call-for-proposal was launched last October. One of the selected proposals is Under Our Sky, an alien-invasion thriller set in Singapore, featuring extensive computer-generated imagery and 360-degree video. The pilot will be released on Toggle early next year.

Developing capabilities for the future

Next, on capabilities. Mr Darryl David asked how we are helping the media sector build capabilities for continued growth. This includes harnessing immersive media technologies like Augmented Reality (AR) and Virtual Reality (VR).

Under IMDA’s partnership with Discovery Networks Asia Pacific, Discovery had commissioned a series of VR titles. The first two titles will be launched later this year.

Another two titles are in the works: Living on the Edge, which tells the story of child Muay Thai fighters in Thailand, and Wild Treks, which shows the life of the Bajau Laut, or Sea Gypsies, in Sabah. These titles are produced by local media companies BeVRR and iMMERSiVELY. They will work closely with Discovery to bring their productions to a global audience through Discovery’s VR app. 

IMDA will be working with game technology company Unity to jointly launch Unity Incubator Services at PIXEL Studios. Our companies will benefit from the business and technical workshops designed for facilitating knowledge transfer and sharing best practices in the development of immersive media applications and games.

IMDA will introduce more accelerator and training programmes to support our companies. I assure Mr Darryl David that smaller companies will also benefit from these programmes.

An example is goGame, a Singaporean mobile game publishing company and a subsidiary of SEGA.  It has partnered with IMDA to launch Singapore’s first game accelerator. The accelerator will train early-stage game developers in business strategy, licensing and marketing.

Another key focus is to invest in our young. With the support of IMDA, Mediacorp has invested in efforts to build a pipeline of young creative talent through apprenticeship programmes and collaborations with schools. 

For example, the Chinese-language web drama series, A Lonely Fish, which was released a few days ago on Toggle, was born out of a scriptwriting workshop co-organised by Mediacorp, Ngee Ann Polytechnic and NUS. Three Ngee Ann Polytechnic students, Oh Wei Ting, Tan Lipei and Ong Wenyi, participated in the workshop and came up with the idea for the series. Under the guidance of Mediacorp’s scriptwriters and producers, they went through a full production cycle, learning the ropes of scriptwriting, acting and production. 

Mediacorp also partnered with Temasek Polytechnic, Republic Polytechnic and Ngee Ann Polytechnic on a Young Creators Project. Under this initiative, experienced mentors from Mediacorp will guide participating students in content production. Mediacorp has selected 11 student projects to commission, which will launch on Toggle later this year. 

Our emphasis on nurturing our young underscores an important point. We cannot build effective capabilities without focusing on talent development. The human touch remains essential to the media sector, whose work centres on connecting with people. 

This applies to our mainstream media too. I agree with Mr Ganesh Rajaram that talent development is key to quality journalism. It is an important element in supporting the mainstream media’s role to provide accurate and objective news reports, as well as insightful opinion pieces and documentary programmes. 

Beyond the newsroom, we are also focused on developing our local talent pool for the industry. IMDA’s ongoing Talent Assistance scheme has been a useful initiative, providing funding support for our media talent to upgrade their skills. From May this year, we will expand the scheme to support media professionals outside of the media sector, such as those who are employed in the manufacturing and retail companies.

At last year’s COS, we announced the Media Manpower Plan to develop a future-ready media workforce. IMDA has made good progress on the implementation of this plan. 

I had earlier announced that IMDA and SkillsFuture Singapore are jointly developing the Skills Framework for Media. This is expected to be launched in November 2018 and will cover about 140 media job roles in areas such as games, broadcast and film. To date, 40 local and overseas stakeholders, including Beach House Productions, Fox Networks Group and Ubisoft, have provided inputs to the framework.

Recognising that media freelancers play an important role across the economy, IMDA launched the Tripartite Standard on Procurement of Services from Media Freelancers in November last year. Since then, 52 organisations have adopted the Standard. 

Starting from 1 April 2018, organisations must adopt the Tripartite Standard in order to qualify for IMDA’s media grants and funding for PSB content. I look forward to more organisations coming forward to adopt the Standard over time. 

Exploring new channels

Let me now touch on the need to develop effective channels to reach our viewers.  I agree with Mr Vikram Nair that we should extend the reach of PSB programming to engage more Singaporeans. Besides traditional free-to-air channels, IMDA has been actively building partnerships with media players in the digital and online space.

An example is IMDA’s partnership with SPH to produce and distribute short-form videos through SPH’s digital content network. The pilot run last year saw the release of 118 short-form videos. Building on its success, SPH will be producing a second slate of digital PSB content comprising more than 120 short-form videos.

IMDA also partnered Viddsee, a Singapore-based online video platform specialising in short films, to deliver more engaging PSB content for the younger audiences on its platforms. This is a good example of a company which is using data analytics to drive viewership and monetisation. 

Viddsee will produce five new original series in collaboration with local filmmakers. The first, the short film Run Chicken Run by Ellie Ngim, was launched last month. Its heart-warming story set during the Chinese New Year period was well-received.

IMDA will also continue working with Mediacorp to better engage viewers on its digital platform, Toggle. Since its re-launch in April 2015, Toggle has seen a steady increase in its viewership to achieve an average of more than 8 million monthly video views today. This is an effort we should continue.

I am glad that Mediacorp’s proprietary analytics tool, RIPPLE, won three BIGGIES Awards at the global Big Data for Media Conference last year, including the “Best in Data Analytics” award. RIPPLE generates useful insights to guide the development of editorial and entertainment content. 

Mediacorp will launch 34 new Toggle Originals this year, up from the 13 released last year. I believe these new titles will offer more entertainment options for our viewers.

Pursuing meaningful collaborations

The fourth and final “C” is collaboration. 

The need for collaboration applies to our mainstream media companies SPH and Mediacorp too, as Mr Ganesh Rajaram pointed out. 

After a successful two-year collaboration, SPH and Starhub recently renewed their partnership for another two years. They will continue their tie-ups in areas such as cross-media content creation and publishing, data analytics and marketing, where SPH-produced content have been broadcast and promoted across StarHub’s pay TV service. Additionally, they will collaborate in their non-media business, to encompass new areas such as healthcare, retail and education.  

Mediacorp and SPH have also explored possible areas of collaboration. They launched a joint digital advertising marketplace, Singapore Media Exchange (SMX), consolidating their data resources to unlock new advertising opportunities and provide advertisers richer targeting capabilities. Both companies are also working with IMDA on joint efforts to market the opportunities in Singapore’s media sector, to better develop our talent pool.

These are some examples of how through partnerships our media companies can position themselves for a digital age and create more value.

Enhancing translation standards

Mr Chairman, I thank Mr Low Thia Khiang for giving us an opportunity to update on the National Translation Committee’s work since its formation in 2014.