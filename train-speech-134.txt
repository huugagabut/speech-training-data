Recognising Digital Tech Innovations

Since 1982, SiTF has been an important partner in promoting technology and innovation. By championing the transformation of the infocomm media sector, SiTF has helped to ensure that the industry stay at the forefront of developments and are able to meet the demands of the economy. 

One of your key initiatives is the SiTF Awards. Inaugurated in 2009, the Awards celebrate many Made-in-Singapore infocomm innovations. This year, the SiTF Awards are extended to companies that have taken efforts to innovate their businesses through the adoption of technology. For this, I would like to commend the SiTF for recognising the impact of new disruptive technologies and the changing technology landscape. 

From 2018, the annual SiTF Awards will be merged with the biannual National Infocomm Awards that is organised by IMDA. This is to streamline the efforts to promote digital tech innovations in our industry. I am confident that the new awards will be the single highest accolade for technology innovation in Singapore, giving more prominence and recognition to outstanding industry players and innovations. 

Prelude to ICM ITM

Over the past few years, we have witnessed how digital technologies brought about changes and opportunities to many industries. The technologies will also have a significant impact on domestic-oriented sectors such as Retail, Accommodation, F&B, and Admin & Support. To better support these sectors, the Government has launched the Industry Transformation Programme to help industries create new value and drive growth.

As part of Transformation Map for the Infocomm Media Industry, IMDA will help all 23 sectors to adopt digital technologies. In so doing, IMDA will enable companies to enhance their operations, develop their workforce and even internationalise their businesses. New technologies such as the Internet of Things, Artificial Intelligence, Data Analytics and Immersive Media will be used to support the other sectors while helping the ICT sector transform. I will announce more details next week.

There will of course be challenges in our digital transformation journey. This is where trade associations and chambers, such as SiTF, play an important role in enterprise upgrading and industry development. With your in-depth understanding of industry needs and growth opportunities, as well as wide business networks, I see SiTF stepping up as an industry enabler in three areas: building business capabilities, enhancing corporate social responsibility and developing the talent pool.

SiTF’s role in building capabilities

First, SiTF can play an active role in building business capabilities to help other sectors and end-user companies. 

Today, there are various assistance available to help SMEs develop capabilities and grow their business. One of IMDA’s key initiative is SMEs Go Digital. It aims to help SMEs build digital capabilities and adopt digital technologies, by offering them off-the-shelf solutions, tech advisory service, opportunities for co-development of pilot and funding support.  

As part of SMEs Go Digital, IMDA is working with SPRING Singapore, industry partners such as SiTF, and larger organisations to collaborate with SMEs so as to help them improve their capabilities and uplift the productivity of the business ecosystem as a whole. IMDA will continue to work closely with SiTF in areas such as curating and facilitating partnerships for SMEs Go Digital to support Singapore’s digitalisation efforts.

Leveraging on its technical expertise and networks, SiTF can catalyse industry transformation by helping to scope and guide the development of group-based solutions among the 23 sectors. 

Forging strong partnerships to enable innovation and growth is also crucial. This is so that resources are efficiently utilised to benefit more enterprises in a synergistic manner, especially in driving solutions that are high in demand to meet common business needs.

SiTF is facilitating collaborations to drive the uptake of technology-related solutions with respective government agencies and trade associations in different sectors, such as Construction. IMDA and SiTF will jointly identify market needs and opportunities, scope technology solutions such as cloud-based platform to digitise processes and core services, and scale up adoption of solutions. Over 300 companies are expected to benefit from this initiative over the next three years.

SiTF’s role in corporate social responsibility

Second, SiTF can exhibit corporate social responsibility. In 2016, SiTF started the Tech4Community initiative, rallying the tech sector to reach out to the community to help them stay connected in this digital age. 

Through partnership with the five Community Development Councils, the programme saw a turnout of over 1200 participants, comprising youth and seniors under the care of Family Service Centres, and 450 volunteers. Participants of all ages were introduced to the basics of coding, and they had the opportunity to experience technologies like virtual reality and drones. The activities triggered interest and raised greater awareness of the digital technologies. I look forward to SiTF’s next Tech4Community event in early 2018.

SITF’s role in talent development

Third, SiTF can play a bigger role in talent development. Currently, SiTF is a member of the TeSA Governing Council and co-chair for the TeSA ICT sector committee with IMDA. The initiative focuses on building and developing a skilled ICT workforce and enhancing the employability of the workers. 

SiTF also recently partnered SPRING in the SME Talent Programme to help local SMEs attract student interns. It also launched the TalentGuru portal to facilitate skills-based career development.  

I look forward to see more of SiTF’s collaboration efforts with government agencies and other industry players to better prepare student for job opportunities, improve mature PMETs’ job opportunities, and encourage hirers to hire base on skills, not just qualification.

Conclusion

The digital economy presents an exciting future – one where Singaporeans can seize opportunities to grow and improve our quality of life.

I would like to thank SiTF for building a cohesive and vibrant ICM ecosystem over the last 35 years. I am confident that for the next 35 years, SiTF will be able to evolve, take on stronger leadership in developing digital capabilities, continue to spur the industry to grow the digital economy and build our Smart Nation.