New Ways of Work is important to Singapore

The New Ways of Work conference comes at an opportune time. Social attitudes are changing, and so is the structure of our workforce. Increasingly, workers are demanding job flexibility to better balance their work-life commitments. Some of them are older workers or women who return to the workforce. But they also include a new generation of workers who are mobile, connected and have higher career aspirations.
 
The structure of the workforce is also changing. Casualisation of labour has increased, as more people turn to freelancing, part-time work and short term contracts.

These changes are happening against the backdrop of technology advancement. Better connectivity has made it easier for us at work; helping us create, collaborate and communicate without needing to be in the same space.

Altogether, these changes are redefining the nature of ‘work’. It is therefore important for businesses to understand and adapt to these trends. Only then will they be able to engage their employees better and maximise productivity. But this is also important for Singapore as a nation because every business and every worker is valuable. With the economy undergoing transformation, we want to support businesses as they progress and support our workers by making every job a good job.
 
Government Supports New Ways of Work

In order for New Ways of Work to take root, there must be a fundamental shift in mindset. We must recognise that the meaning of ‘work’ is changing. It goes beyond where or how work is being done, but what ‘work’ actually is. With increased connectivity, businesses can be run from anywhere around the world and work can be done at any time of the day. For some people who feel a deep sense of purpose in their jobs, work is very much intertwined with their life and leisure time. 

For companies, they need to acknowledge that flexibility and productivity can go hand in hand. A survey of workers globally found that three-quarters say they are more productive when they work flexibly[1].
 
Most importantly, a mindset shift can only take place when a culture of trust is established between employers and employees. Instead of getting employees to clock hours, employers need to shift towards a more outcome-oriented approach. Employees too have to play their part in meeting work targets and expectations.
 
The Government is working with partners through the TriCom to bring about this mindset shift. In November 2014, the TriCom launched the “Tripartite Advisory on Flexible Work Arrangements (or FWAs)”[2] to guide employers, supervisors and employees. Over the last year, the TriCom has launched work-life outreach campaigns to reinforce how FWAs benefit both employers and employees. It has also produced publications such as “Work Happy”, which features 50 companies that have been successful in their work-life practices. We will continue to build on these efforts, and provide resources to help companies implement FWAs, including through grants such as the Work-Life Grant.

The Public Service is one employer that champions FWAs. Public agencies offer FWAs such as telecommuting, part-time work arrangement and staggered start-work hours. These agencies have put in place guidelines on flexible work and where applicable, equip their officers with laptops and Virtual Private Network (or VPN) tokens to do their work off-site. Such arrangements have been useful to certain groups of officers, such as mothers.  In a 2015 survey[1], one quarter of respondents who are mothers said they have telecommuted, including 10% who do so at least once a fortnight.

Enabling New Ways of Work through Technology and Alternative Workspaces

Another key focus area is harnessing technology to support New Ways of Work. Good infocomm infrastructure allows our workers to be connected to their offices remotely. It is also essential for services like mobile video conference and cloud-based applications. Over the years, we have been putting in place infocomm infrastructure that allows pervasive and seamless connectivity for everyone, everywhere, anytime. This includes the Nationwide Broadband Network and enhancements to Wireless@SG. More recently, we have conducted the trials for a Heterogeneous Network (or HetNet) that will provide faster and smoother data connectivity. The results of the trials have been positive.
 
We also look into new technology areas such as data analytics, Internet of Things and robotics. These may help companies in building better, smarter and more personalised workspaces. In turn, this will improve the ways employees work and collaborate with each other.
 
With improved infocomm infrastructure, we were able to pilot Smart Work Centres (or SWCs) at public libraries 2 years ago. I understand they have been successful. Month after month, the SWCs are almost always fully occupied[3]. In particular, the Toa Payoh SWC is so popular that it is full house every month for the last one and a half years[4].
 
Workers from a wide range of industries and job functions are using SWCs. There are users from industries such as IT, PR and marketing, and even F&B and construction[5]. The types of work they do include administrative work, sales and design[6]. It is heartening to note that out of every 3 users, 2 are either self-employed, freelancers or from a start-up[7]. This shows that alternative workspaces are appealing to the changing workforce that I mentioned earlier.  
 
Feedback has also been positive. A majority of users have found the SWCs beneficial[8]. Employees like these SWCs as they are near to their homes, making it easier for them to manage their work and family needs. For example one employee, Mr Preston Koh, a marketing manager at Asia Air Movement and Control Association Pte Ltd, the SWC helps to reduce his commute time tremendously. He used to travel to Johor every day since his company’s laboratory moved there. But now, he is able to work out of the Toa Payoh SWC, which is much closer to where he lives.
 
For employers, they are happy that these SWCs help balance their business needs and the needs of their employees. Take for example one SME, Realtimme (read as ‘real time’) IT Consultancy, a cloud-based accounting software firm. For its staff, a large part of their work involves meeting clients for sales and technical support. It is important for them to have a convenient place of work without needing to travel to and from a central office. This is where the SWCs come in, by providing workspaces around the island and helping Realtimme staff improve their productivity. For the owners, they also benefitted from not needing to spend resources on office rental and maintenance. This helps them to save cost and focus on the more important aspect of developing their core business.
 
In view of the positive feedback, we will continue the current SWCs at Toa Payoh, Jurong East and Geylang East. In addition, we will open a new SWC by next year and it will be housed in a revamped Tampines Regional Library. We hope more companies and employees will come on board and enjoy the benefits of these alternative workspaces.

The infocomm infrastructure we have put in place has also enabled the private sector to develop innovative alternative work places and workspaces catering to specific needs. For example, there is Trehaus, a co-working space that comes with child minding services, to meet the needs of working parents. There is also the Working Capitol, a space designed for the business community and entrepreneurs looking for creative collaborations. The Government will continue to support these new developments. We will also explore other community buildings as new work centres. Ultimately, our aim is to help our businesses find ways to enhance their productivity and help our workforce improve work-life quality.

Conclusion

With the new generation of young workers or millennials in our workforce today[9], our strategies for talent attraction and development must adapt to meet their needs. New Ways of Work play a vital role in this. So I urge all of you here today to gain a deeper understanding of its development and seize the opportunity to adopt new practices for your organisations. If we can fully embrace the New Ways of Work, Singapore will remain a strong talent hub. I wish all of you a fruitful conference.