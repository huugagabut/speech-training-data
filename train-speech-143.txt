Singapore will enhance growth strategy for the digital economy; commits to being industry’s partner-in-innovation



Opening: Be unbound to innovate

	Singapore is proud to host innovfest unbound. The event name is fitting –innovation often requires us to be unbound by inherent constraints. In many ways, being unbound by constraints is also the Singapore story.

Singapore’s founding fathers were acutely aware of our constraints – we are small and we lack natural resources. When Singapore became independent in 1965, we had a population of 1.9 million people and a nominal GDP per capita of about $500 USD, but these conditions never deterred us from punching above our weight in the global arena. Dealt with such a hand of cards, we had to be creative and innovative in building the Singapore economy. We opened our economy to external markets, as our forefathers envisioned Singapore to be a meeting point for conducting business between the East and the West, Singapore as a global business hub.

Being unbound by size, resources constraints and borders, we developed a growth strategy that has served us well in that economic era. But we are entering into a different era today, where our world of mainframes and desktops is making way for cloud and the digital economy. Singapore needs to innovate and enhance our growth strategy, to overcome our physical limitations and bring good jobs and opportunities to all Singaporeans in the digital age. No Singaporean will be left behind.

Singapore will enhance our growth strategy for the digital economy

As part of Singapore’s roadmap to become a Smart Nation, our enhanced growth strategy for the digital economy will go hand-in-hand with our digital government strategies. In fact, as we speak, the Singapore Head of Civil Service Mr Peter Ong is currently making a speech about digital government at the Digital Government Exchange! Indeed, both the public and private sectors have important and complementary roles to play. Let me now outline three key thrusts of our enhanced growth strategy for the digital economy.

Thrust 1 – Invest in building frontier capabilities

The Singapore Government has never shied away from taking calculated risks in investing in the future, as evidenced in the Singapore story. For the digital economy, it is imperative that we make strategic decisions on where and how Singapore will harness frontier technologies to develop capabilities that will power our future economy. These technologies have begun to disrupt and more importantly redefine the ways we live, play and work. They also bring immense opportunities for growth, if we could harness them and build new capabilities.

During my Committees of Supply, or COS speech in March, I mentioned that we would be increasing our investments on frontier technologies such as Artificial Intelligence and Data Science. AI has emerged due to a confluence of three factors: the availability of Big Data, advances in high-performance computing and the invention of new learning algorithms and architectures. The potential gains from an enabler technology like AI are massive.

Today, I am happy to announce the establishment of AI.SG, a new national programme to boost Singapore’s AI capabilities. AI.SG will be driven by a partnership comprising the National Research Foundation, Smart Nation and Digital Government Office at the Prime Minister’s Office, Economic Development Board or EDB, Infocomm Media Development Authority, or IMDA, SGInnovate, and the Integrated Health Information Systems, or IHiS.

AI.SG will do three key things – First, address major challenges that affect society and industry, second, invest in deep capabilities to catch the next wave of scientific innovation, and finally, grow AI innovation and adoption in companies, an initiative most pertinent to the business community. We have identified three focus areas of application for AI.SG – finance, city management solutions and healthcare.

For example, in the area of healthcare, we will soon be able to benefit from various trial applications of AI by IHiS and their partners. In about three years’ time, doctors and nurses could find themselves immersed in simulated emergency situations, and interact with virtual patients in realistic augmented reality-based environments to enhance their training for better patient care.

I am also happy to announce that NRF has collaborated with the National University of Singapore, Nanyang Technological University, Singapore Management University and the Agency for Science, Technology and Research, or A*STAR, to set up the Singapore Data Science Consortium. The consortium will strengthen collaborative research linkages between Institutes of Higher Learning, research institutes and industry. This will help industry to adopt data science and analytics technologies to address real-world challenges. I look forward to more demand-driven AI and data science innovation and adoption in Singapore.

AI and data science are key frontier technologies that the Singapore Government will harness and build capabilities in. In the longer-run, such investments will enhance the economic opportunities for all Singaporeans. In the upcoming months, I will speak more about other key frontier technologies that Singapore will invest in as part of our enhanced growth strategy.

Thrust 2 – Support promising enterprises

Our second thrust is to help promising Singapore-based tech-product companies. Accreditation@IMDA, a key initiative launched in 2014, collaborates with SG Innovate and industry accelerators to provide customised assistance to high-growth start-ups and SMEs to accelerate their growth, and to internationalise through IE Singapore and our enterprise partners. 

For example, Vikram Mengi, co-founder of data analytics company Latize, has always found it a challenge to get decision makers to spend time understanding their product before they were accredited. After getting accredited, Vikram said that it has become much easier to do so. Vikram and his team was also able to close their first funding round much faster, as investors relied on IMDA’s Accreditation evaluation for their technical due diligence. The success of their fund raising, coupled with the track record they have built through the Accreditation process, has helped them to expand and establish their first overseas offices in Sydney & Perth; they have since won two projects in Australia. To date, Accreditation has accredited 17 companies. Accreditation will continue to help promising enterprises like Latize grow and internationalise.

Accreditation will progressively establish key partners in various sectors to help accredited companies scale up more effectively. Today, I am pleased to announce IMDA’s partnerships with DBS, OCBC and UOB, which have been very active in creating and supporting innovative FinTech solutions. Through these partnerships, Accreditation@IMDA will support the banks in driving innovative FinTech solutions within their organisations and sector. The partnerships will also enable IMDA-Accredited companies to scale up, by gaining access to real problem statements and opportunities to work on innovative FinTech projects within the banks and expand into international markets through the banks’ global footprint.

As with start-ups, the Singapore Government will also continue to provide customised support for SMEs to seize opportunities in the digital economy. In my COS speech, I talked about how SMEs Go Digital will help raise SMEs’ overall level of digital readiness to help them at various stages of their digital transformation journey. By today, I am pleased to hear that SMEs Go Digital now has close to 50 pre-approved digital tech solutions that SMEs could tap on to innovate.

An example is a 3D Internet-of-Things platform for Smart Buildings solution by Asahi Security Technologies. This security solution helps building and business operators to better manage and control situations, and improves decision making through 3D visualisation in the command centre. SMEs Go Digital will continue to support SMEs to transform in their digital journey, be able to embrace innovation and scale up.

Thrust 3 – Develop a pipeline of frontier tech talents

Our third thrust builds on one of Singapore’s founding strength – talent development. As we enter into the digital economy, Singapore will need to build a strong pipeline of digital scientist with skills in frontier tech areas such as AI. Through our engagement with the industry, we are keenly aware that a steady pipeline of quality tech talent is critical for businesses to break new frontiers in the digital economy, to grow and scale the businesses, especially as your time-to-market continues to shorten in the tech industry.

At COS this year, I reiterated the importance of tech talent development when I talked about the good work that IMDA’s TechSkills Accelerator, or TeSA, is doing. As part of this thrust, TeSA will continue to equip the Singaporean workforce with tech skills for the digital economy. In areas of frontier technology where we have yet to build deep capabilities, we will continue to be open to global talent. Together, we can build a pipeline of digital scientists who will help businesses at all enterprise levels to hasten your speed-to-market in the digital economy.

Beyond manpower, Singapore will continue to enhance other aspects of our ecosystem, including first-class connectivity infrastructure and a conducive regulatory environment, to support the innovation journey of all enterprises. We know the way ahead would not be easy. Hence, it will be all hands on deck for the government to help businesses succeed in the digital economy. What I have outlined today will be complemented by efforts from many other agencies, such as Startup SG’s Founder initiative, which will encourage first-time entrepreneurs to start new businesses in frontier tech areas, and SG Innovate’s BASH, Singapore’s all-in-one facility to help build tech communities and networks across the entire value chain. 

Conclusion: Singapore government will be industry’s partner-in-innovation for the digital economy

Hence, events like innovfest unbound are critical as Singapore innovates and enhanced our growth strategy for the digital economy, to bring together leading entrepreneurs, brands, corporates, investors and tech start-ups from around to world to expand networks, share experiences, and showcase ideas. I have no doubt that we will see the most innovative tech ideas here, igniting a passion for innovation among Singaporeans young and old, from all walks of life.

This year’s innovfest unbound features four additional satellite events to highlight the trends shaping the word of tech. I am sure that you will make plans on how best to harness these tech developments for your endeavours, as will the Singapore government. Let us be your partner-in-innovation in this journey, as we charge ahead together in the digital economy.