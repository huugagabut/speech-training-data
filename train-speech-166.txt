	Data is the “new oil” of the 21st Century. That is why, when Singapore launched the Infocomm Media 2025 last year, one of the strategic thrusts was about capitalising data to make our businesses smarter and our economy more competitive. At the same time, the collection and the use of data must rest upon a foundation of trust.

It is hence opportune that the Personal Data Protection Commission (PDPC) is organising today’s Personal Data Protection seminar to discuss issues surrounding big data and how trust and innovation must go hand in hand.  

Data as Opportunity

Friends, ladies and gentleman, developments in technologies have dramatically disrupted business operations and reshaped how businesses run their operations and serve their customers. Through the use of data, businesses have been able to understand consumer preferences at a deeper and a more personal level. And this translates to more customised products, better marketing and loyalty programmes, and smoother delivery of services. 

So take for example DBS, one of our big local banks, which was recently named the world’s best digital bank by Euromoney. The bank uses data analytics to determine how it could better serve its customers.  So to avoid the long queues that typically form at the ATMs before the Lunar or Chinese New Year, as customers withdraw money for what we call the ‘red packets’ – something that you give to children when they come to your house for visits, the bank used aggregated data to identify the highest traffic spots. So last year, it introduced "pop-up" ATMs at these high traffic spots. And these pop-up ATMs were so well-received that they were back again earlier this year. 

DBS also recognises that transforming the workplace culture and exposing them to the digital concepts can help optimise data opportunities further. So it has introduced hackathons to its employees to encourage them to embrace a digital mindset and leverage on data to innovate.

Data Protection by Design

Current times offer us many exciting opportunities to better interact with our customers. So to better seize these opportunities, we also need to ensure that customers feel safe interacting and transacting with us. It is no longer an option to treat data protection as an afterthought. Businesses who do this have learnt the hard way that trying to win back customer trust is far more challenging, and sometimes expensive, than trying to win a new customer. 

This is where Data Protection by Design comes in - making data protection a key consideration when we build our business structures and systems. With Data Protection by Design, the people are important as well. Employees should be aware, and also be trained to take responsibility to safe-guard customers’ personal data.

That is why we have made it mandatory for organisations in Singapore to appoint Data Protection Officers or DPOs under the Personal Data Protection Act (PDPA). Even while DPOs ensure that their organisations have proper processes in place for data protection, they are also well-placed to share best practices with their colleagues on how to manage and make best use of data to improve operations or to seize new business opportunities.  

RedMart, an online grocery service provider, is a Small Medium Enterprise (SME) that has designed its business from the onset with data protection in mind. Today, more people are shopping online. But there are customers who continue to be cautious of online shopping and how their personal data will be used.  So to assure its customers, RedMart has designed its operations around personal data protection. Its mobile app discloses only relevant personal data for each stage of order processing. And when a delivery representative needs to contact a customer, the call will be made through the app without revealing the customer’s phone number to the delivery representative.

Chan Brothers Travel, a travel company, is an example of a company that has appointed and made the most out of their professional DPO.  Ms Janet Chan has put in place a system for customers to state their preferences at the point of purchase – down to whether they would be all right with having videos or photos taken of them on tour. Janet also conducts regular training to keep employees up-to-date on data protection policies and practices. 

Professional Development for Data Protection Officers (DPOs)

So DPOs, like Janet, have an important and professional role to play in their organisations. And they have much to look forward to as well in terms of professional development.

Organisations are now gaining awareness of the role of the DPO in enabling responsible data sharing. The recent annual survey conducted by the PDPC shows that close to half of the organisations surveyed this year have appointed a DPO, a significant increase from the one quarter last year.  We understand that SMEs need time to develop this capability. PDPC has put in place several initiatives to help companies build this up and comply with the requirement to appoint a DPO as soon as possible.

The PDPC has also introduced initiatives such as industry briefings for companies, e-learning modules, and a self-assessment business checklist. These are all aimed at helping the DPOs gain a deeper understanding of data protection concepts, which they can apply to mitigate and solve practical problems related to handling personal data. 

The PDPC is also working with the Workforce Development Agency (WDA) to enhance the current two-day course for DPOs. Besides the inclusion of additional topics on international data protection frameworks, data breach management and enforcement, the enhanced course will focus on developing the practical skills of DPOs, through rigorous assessments and in-class participation. 

To help SMEs improve their data and business risk management capabilities, the PDPC will facilitate SMEs to tap on the SPRING Capability Development Grant (CDG). This would enable companies to defray up to 70 percent of qualifying costs, such as consultancy and training, assessments and audits, and adoption of data protection software solutions.

Conclusion 

Given the importance of data, the Committee on Future Economy is also looking into how Big Data can be used more effectively by our businesses, and what structures and systems we need to put in place to harness economic value from the data. Minister of State, Dr Janil Puthucheary, is leading a group to study this matter more deeply.

Leveraging data for business opportunity and other competitive advantages does not happen by chance.  One needs concerted effort and conscientious planning to design the business operations and organisational processes to collect and manage the data, and also protect it, before one can capitalise on data effectively for opportunities and growth. It starts with the people in our organisations – their awareness and their sense of ownership towards being custodians of data. 

So I urge all leaders to play an active part in steering your businesses and organisations towards better data protection.  And I believe the discussions and sharing from this Personal Data Protection Seminar will provide the insight and learning that will enable us to manage, protect and use data more effectively.  