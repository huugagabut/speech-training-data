	It is an honour for me to be here today to launch the Singtel Cyber Security Institute. As Singapore moves towards being a Smart Nation, one of the most pressing challenges we face is protecting the infrastructure which forms its foundation. While the Smart Nation will open up many exciting opportunities for us, it will also mean that we have to be more vigilant to cyber threats, which are growing exponentially in scale and complexity.

As we embrace the digital economy, we must not forget that cybersecurity is an important enabler that will give us the confidence to interact and transact digitally. Cybersecurity professionals work tirelessly behind the scenes at our telcos, banks, and utility companies to ensure that the critical services their companies provide are safe from cyber attacks. We have these cyber experts to thank for keeping watch over the systems and networks which keep Singapore going.

Two challenges

However, cyber attackers keep getting smarter. Reports of incidents and breaches are now commonplace, and governments and businesses find themselves fighting to stay one step ahead. Cyber attackers also have no respect for borders – they can be anywhere, coordinating attacks with fellow perpetrators on the other side of the globe. The volume and sophistication of cyber threats today means that cyber professionals have to perform at the highest level possible.
 
The situation is a serious one, and there is no quick solution. Our need for cyber experts with deep skills and high levels of expertise will only increase. Our challenges are therefore two-fold: First, we have to ensure that we have the manpower; and second, that they are well-equipped with the skills and know-how to confront not just today’s cyber threats, but those that the future will bring.

To address these challenges, we will need to cultivate a vibrant cybersecurity ecosystem, and develop strong cybersecurity talents.

Cultivating the cybersecurity ecosystem

Our vision of a dynamic cybersecurity ecosystem is one that comprises strong local cybersecurity firms co-existing with established global companies, fed by a talented workforce. Across the economy, businesses must also be sensitive to cyber threats and adopt the right level of cybersecurity measures. Within this ecosystem, cybersecurity talents can circulate, providing the spark for greater innovation.

This is why the Cyber Security Agency of Singapore and the Infocomm Development Authority have been working in partnership with the private sector on the Cyber Security Associates and Technologists Programme, or CSAT. I first announced it at last year’s GovernmentWare conference, and am glad to share more details with you today.

CSAT is a dual-track programme: the Associate track is for fresh ICT and engineering professionals, and the Technologist track is for experienced professionals seeking to make the switch to cybersecurity. Under CSAT, both these groups can be upskilled through on-the-job training programmes led by companies which are CSAT training partners. These professionals can be trained for roles encompassing security assessment, security operations and the technology development of cybersecurity solutions. They will have the chance to take courses, and participate in local and overseas attachments identified by the CSAT training partners.
 
Trainees can apply their newfound skills to projects, and learn from the best through mentorships with experienced industry practitioners. The comprehensive training available through CSAT will help raise the standards of cyber experts, as well as strengthen the professionalism of the sector.
 
I would like to congratulate Singtel for qualifying as a CSAT partner. Singtel has been making its presence felt in the cybersecurity arena, and I am glad that they will be contributing to developing our cyber professionals’ skills. I am also pleased to note that a number of other companies, including Ernst & Young, NEC, Quann, Palo Alto Networks, and ST Electronics (Info-Security) have shown interest in being CSAT partners and working with us to grow the pool of cybersecurity experts in Singapore.
 
New skills, new paths

I spoke earlier about the evolving nature of cyber threats. In an operating environment which is constantly in flux, our cyber experts must be nimble and adaptable. Even as they confront the problems before them, they must also be forward-looking, able to anticipate the shape of future threats, and ready to equip themselves with new tools and new knowledge.

To that end, I am happy to note that Singtel has brought together over 30 ICT and cybersecurity companies as partners for the Cyber Security Institute, which will be equipped with state-of-the-art facilities to prepare cyber professionals to deal with today’s threats, while allowing them to anticipate emerging ones. At the Institute’s cyber range, I understand that cyber professionals can also pit their skills against adversaries in realistic simulation environments. Exercises like these can help heighten cyber professionals’ knowledge of their roles and their responsiveness during cyber incidents.
 
Besides equipping cyber professionals with skills to deal with the problems of today, I am happy to note that this Institute will also help prepare them for tomorrow’s threats. Its incubation lab will focus on scanning for emerging and disruptive cybersecurity solutions. The lab will also enable the testing of proofs-of-concept, and the validation, adaptation, integration and eventual commercialisation of new solutions.

Recognizing our cyber professionals
 
Cybersecurity needs good men and women, and many more of them. Just two weeks ago, Deputy Prime Minister Teo Chee Hean announced a 20-percent increase in starting salaries for engineers joining the Public Service where we will take the lead. He also highlighted that the government would pay a premium for those with skills in malware analysis and cyber forensics, which are in high demand. With this, I hope that those with an interest in cybersecurity will feel that they can realise their passion in a field where the rewards are commensurate to the importance of the work they do.

Coaching the C-suite
 
However, it is not enough simply to train the troops on the ground. While it is important to hone the capabilities of those on the frontline, those in command must also be aware of the challenges we face in a digital economy. Only then can they make good decisions on how best to protect our critical information infrastructure. Just two weeks ago, I spoke in Parliament during the Committee of Supply debate on how the C-suite needs to make cybersecurity a priority. I note that the Cyber Security Institute is also taking a step in this area by tailoring programmes for the C-suite, where they can be trained in crisis management, governance, and risk and compliance frameworks. 

Conclusion
 
Once again, I am happy to be here to launch the Singtel Cyber Security Institute. I believe that institutes like this one will provide a valuable space for cybersecurity professionals to come together to sharpen their skills and stay agile in this dynamic environment. I hope current and aspiring cyber professionals will take up the challenge, and help make Singapore a more secure and resilient Smart Nation.