Contributions of the Audio Post

When we watch a drama or a movie, we often focus on the actors and storyline. These are very important. But the music and sound effects can also leave a deep impression on us. Sometimes, we are reminded when we hear the soundtrack of the movie years later. For example, many of us grew up with the music that Mediacorp’s Audio Post produced for our popular TV dramas. I still remember the theme song of the blockbuster drama, 雾锁南洋 or “The Awakening” which I watched as a child.  


With just a team of fifteen, the Audio Post has punched above its weight. Besides TV dramas, you have ventured into movies.  As mentioned earlier by Mr Cheah Chee Kong (Mediacorp’s Chief Content Officer), “Ah Boys to Men” is one of the many movies that you have participated in and done very well. You saw the need to constantly improve and adopt new technologies to bring better quality content to your viewers. To the Audio Post team who has been working hard behind the scenes, supporting our producers and our actors, I would like to say thank you and please keep up the good work. 


A boost to Singapore’s media eco-system

These are exciting times for the media industry. Some of our home-grown talent have received international accolades for their productions. Earlier this year, Singaporean sound editor, Ms Lee Ai-Ling, was nominated for an Oscar for her work in a Hollywood movie. Sound designer Lim Ting-Li, who is an Infocomm Media Development Authority (IMDA) scholar, has also done us proud. Ting-Li has won international awards for sound design, and worked on many local films including Kirsten Tan‘s Pop Aye and Boo Junfeng’s Apprentice. Their success stories have inspired many others and we are seeing greater interest among our young to join the film and media industry. Mediacorp’s new Movie Mixing Studio will add on to this increasingly vibrant media eco-system.  


The new studio will offer film-makers access to the latest technologies, which are built to international standards. I hope that one day, this will become the choice studio for movie makers in the region and all over the world, and a Singaporean will receive an Oscar for sound editing done from here. 


Continued partnership in content and talent development

I am glad that our Public Service Broadcast (PSB) funding has supported many local independent production companies to create quality content for Singaporeans. In addition, the PSB Contestable Funds Scheme has enabled our companies to enhance their storytelling and production capabilities, to produce innovative programmes to engage audiences on our Free-To-Air channels and online platforms like Toggle.  I recently went into Toggle. I think you have some changes to your format which is very good. It is easier to navigate and I noticed you have highlighted some of the older TV shows to help audiences who want to reconnect to what they had watched previously. This is not possible to do on Free-To-Air TV but on online platforms like Toggle, there is a lot more scope to add such features. So well done to the Toggle team. Please keep it up as well.


Our media companies also benefit from collaborations with international partners to produce quality content which can be enjoyed by local viewers and exported overseas.  Some examples include IMDA’s partnership with Discovery Networks Asia-Pacific in October last year, which aims to develop local talent and co-create content with production companies that will premiere on Discovery's platforms globally, using the most advanced techniques and technologies such as 4K and Virtual Reality. In April this year, IMDA partnered Turner’s Cartoon Network to provide training and development opportunities on creation of animation story ideas.   Beyond these big names, I know many of our production houses are also working with overseas partners in Asia which may not be as well-known, but nevertheless, produce very good quality work. I think this has really helped us to also take our ideas, our content overseas, and to bring good ideas from overseas here. My belief is that we must remain connected and open, for ideas to flow, for talent to flow, in order for us to produce good quality content that can be enjoyed both here and overseas.


As our national broadcaster, I would like to encourage Mediacorp to continue supporting these industry development efforts to develop a strong media eco-system in Singapore.  And partner our independent production companies, our studios and our free-lance media professionals, and give them opportunities to show-case their talents.  This is a win-win arrangement for everyone, which also benefits Mediacorp as it will increase the chances of producing quality local content for our viewers to enjoy.                          