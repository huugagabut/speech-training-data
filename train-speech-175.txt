	I will speak about MCI’s priorities in three areas: strengthening Public Service Broadcasting, enhancing government communications and translation, and expanding the role of design in our lives.
 
Public Service Broadcasting for a new age

Importance of Public Service Broadcasting

Public Service Broadcasting, or PSB is an important part of our lives. PSB programmes not only entertain and educate Singaporeans, they also reflect our societal values, celebrate our local heritage, and help to shape the Singapore identity. News and current affairs programmes keep Singaporeans well-informed about the world around us, and unite Singaporeans with shared experiences. For instance, we mourned as one during the National Mourning week last year, when our founding Prime Minister, Mr Lee Kuan Yew, passed away. And we cheered with one heart during our SG50 National Day Parade.

Only locally produced PSB programmes can have such effects. Foreign programmes will not reflect our local values and culture. Through PSB programmes, our media industry players can tell the Singapore story from a Singaporean perspective, with a Singaporean heart.

PSB must be ready for a new age
 
That is why the Government has long supported PSB, and indeed increased our funding for PSB in recent years. Like many other countries – Britain with the BBC, Japan with NHK – we recognise the importance of PSB.

In the last financial year, we provided $250 million of PSB funding, almost double the amount three years earlier. As Mr Ganesh Rajaram observed, this sustained investment has enabled Singaporeans to enjoy many more quality programmes over free-to-air TV and radio, as well as through online platforms like Toggle.

Let me share some examples. Channel 8’s 118, which addresses social topics in a light-hearted way, reaches more than one million viewers every week. Channel 5’s Tanglin is watched by more than 300,000 viewers.  Suria and Vasantham have grown their viewership significantly in the last few years. 
I visited them recently and was impressed by how they have built strong relationships with their audiences, organising community roadshows so the public can meet the stars in person.

Mr Darryl David asked if PSB funds support media organisations other than Mediacorp. The answer is yes. While most PSB programmes are produced by Mediacorp, a significant proportion was outsourced to independent production companies and would continue to be the case.

Since 2013, we have also set aside some PSB funding for independent producers. For instance, this has enabled companies such as WaWa Pictures to produce shows like Crescendo, or 起飞. The show included a live xinyao concert for the public that was featured in the series finale. Through this show, Singaporeans learnt about the development of xinyao and the Chinese music scene here.
 
I am encouraged by the progress that Mediacorp and our media companies have made.  But the operating environment for our media companies is getting more challenging.  Our viewers are consuming content from a variety of sources -- Netflix, iTunes, Spotify and many others. A few members have spoken about this. Mr Ganesh Rajaram spoke about convergence. Indeed we are facing that challenge. Free-to-air TV has to jostle to keep the attention of viewers.
 
Several Members, including Mr Zaqy Mohamad, Mr Vikram Nair and Mr Darryl David, have asked what we are doing to strengthen PSB in a digital age.

We will strengthen PSB by focusing on three Cs: Content, Capabilities and Channels.

Transforming PSB through Content, Capabilities and Channels (3 Cs)

Content

First, content: I have spoken about how additional investments in PSB have improved the quality of PSB content.
 
We will not be able to match the budgets of blockbuster American productions like Game of Thrones, or Korean drama Descendants of the Sun. But we can find other innovative ways to attract viewers – by producing quality local content, by focusing on our cultural heritage and by featuring interesting aspects of Singapore society.
 
We have had several of such well-received programmes – Mediacorp dramas like The Little Nyonya in the past, and more recently, excellent documentaries commissioned by Mediacorp like Wild City – Islands and All Access Changi.  These were produced by Beach House Pictures, an independent production company founded by Mr Donovan Chan and Ms Jocelyn Little in 2005.

Capabilities
 
This brings me to my second point -- building strong capabilities. Media is a talent-driven and knowledge-based industry. PSB funding plays a useful role to support Mediacorp and local media companies to attract, train and retain the Singaporean talent.
 
Besides funding, we want to create opportunities for learning and collaboration. In May last year, we launched Fox Format Labs, a partnership between MDA and Fox International channels. Fox worked with five Singapore production companies to produce content for the National Geographic Channel. Through the collaboration, our companies could screen their shows to an international audience.
 
Last December, MDA and HBO Asia announced a two-year collaboration to raise local capabilities in drama production.
 
I am pleased that the industry has responded positively to our initiatives. Mr William Lim from Xtreme Media said this was “exactly what local producers need”.

We must also continue to raise our capabilities in story-telling, set design and videography. When I visited the set of Tanglin¸ I met its group of talented script writers. They work in a team, constantly learning from one another and also challenging one another to think of fresh ideas. Mediacorp also brought in Ms Kathleen Beedles as a consultant. She is a UK producer who has worked on several successful dramas like the long-running Emmerdale, which has been on air in the UK since 1972.  Working with someone like Ms Beedles gave our local production team and young talents opportunities to learn from experts in their field.

Channels

Third, we must proactively embrace new channels to reach audiences who are consuming news and entertainment through different media. Many Singaporeans today, especially the young, spend a lot of time online. So PSB must extend its reach online to engage and connect with our audiences.

Mediacorp launched Toggle in 2013. Within three years, Toggle’s viewership has grown eight times. The increase in viewership was especially significant after its re-launch in April 2015. I met the Toggle team during my visit to Mediacorp, and congratulated them on their success.  I also urged them to continue looking for new ways to innovate and increase Toggle’s reach.

Toggle is a great platform to experiment with new concepts. Over the next 2 years, Toggle will release a slate of 11 original programmes aimed at younger viewers. The first, A Selfie’s Tale by Oak3 Films, has done well so far with over 600,000 views. PSB funds have also supported Pursuit of Champions, a documentary about young Singaporean athletes, produced by The Moving Visuals. This programme is currently available on StarHub Go. Singapore Airlines also acquired the programme for its inflight entertainment, bringing local content to an international audience.

We must ensure that our channels remain relevant. Mr Vikram Nair asked about Mediacorp’s review of Okto and Channel U. Mediacorp is exploring the idea of positioning Okto as a channel focusing on children and young adults, as well as sports. We have been discussing with MediaCorp the idea of including coverage for school sports. I think there is value in supporting quality programmes for children, as Ms Kuik Shiao-Yin has suggested.  We have a good local children’s programme called Mat Yoyo; it is now available in English.

As for Channel U, Mediacorp is examining ways to further enhance its Chinese content, which is currently delivered through Channel 8 and Channel U. Over the years, our vernacular channels have contributed significantly to the development of bilingualism here. They allow Singaporeans to stay connected to our language, culture and communities. Besides Chinese programming, we will also ensure that Suria and Vasantham have adequate resources to continue producing quality programmes to serve our different ethnic communities.

Ensuring long-term viability of PSB

The successful transformation of PSB will require persistence and hard-work. Our industry players like Mediacorp need to have a steadiness to ride through business cycles, and not allow short-term commercial ups and downs to affect their longer-term goals of producing good quality content, building strong capabilities and developing effective channels.

But the external environment is challenging.  Public broadcasters in many countries face difficulties due to increased competition from online media and falling advertising revenue.  Even the BBC is not spared.  It moved its youth-focused TV channel, BBC Three, entirely online. It also plans to overhaul its organisational structure in response to these challenges.

Besides the BBC, other public broadcasters like RTHK in Hong Kong and NOS in the Netherlands, also regularly review their structure, strategy and funding models.

This is why Mediacorp and its shareholder Temasek Holdings have decided to review Mediacorp’s operations to better position the company for the future.

The Government is fully supportive of this review and we will work closely with Mediacorp and Temasek Holdings in this effort. We want to help Mediacorp to further strengthen the three Cs (content, capabilities and channels), so that the company can continue to do well as Singapore’s national broadcaster, and produce good quality local programmes for Singaporeans to enjoy for years to come.

Enhancing Government communications and translation

I will now touch on the ongoing efforts to enhance government communications and translation.

Mr Zaqy Mohamad and Mr Ong Teng Koon asked about further strengthening government communications for different groups of Singaporeans.  This is a priority for the Government. We believe that communications is an integral part of policy formulation and implementation.  We have built up stronger communication capabilities over the past few years.  Looking ahead, we will continue to focus on three key areas: Content, Platforms and Languages.

Content that connects

First, we need to create engaging content that can connect with Singaporeans.

The most powerful and effective messages are simple to understand and contain a strong emotional element that people can relate to. MCI worked with Mr Jeff Cheong and his team from Tribal to pilot a new approach for our recent MediShield Life video, Love of a Lifetime.  It had one simple message – just as the love between a husband and wife is lifelong, so too is the lifelong coverage of MediShield Life.  The video has garnered more than 4.5 million views on social media, and was much talked about during Chinese New Year when it was launched.

Multiple platforms to reach different groups

Next, MCI will use multiple platforms and channels to reach different segments of the population. Last year, we revamped Gov.sg and optimised it for mobile devices because that is what more people are using these days. We are now looking at Instagram and Telegram to supplement our communications.

At the same time, we recognise that online platforms are not for everyone. Face-to-face interactions will continue to be a key part of government communications.  The human touch is important.  A good example is how Pioneer Generation (PG) Ambassadors communicate with the elderly.  This has been very effective. In 2015, awareness of the PG Package among Pioneers reached 96 percent.

We want to help our seniors to understand how they can benefit from different government policies and measures. We can build on the PG Ambassadors network and expand it to reach more seniors beyond Pioneers, and to explain a range of government policies beyond the PG Package and MediShield Life.

Translation of languages: Raising awareness, interest and standards

Let me now touch on the importance of raising translation standards.  I agree with Mr Ong Teng Koon that this is important to strengthen government communication in Chinese, Malay and Tamil.

The National Translation Committee (NTC) has rolled out various initiatives in the past two years to strengthen translation capabilities in Singapore. For example, we started the Information Service (Translation) Scholarship to groom more translation talents for the future.

It is important to nurture passion from young, and help our students to see the relevance of translation in their daily lives. We will work with schools and community partners to reach out to more young Singaporeans.

Growing the design sector to boost our future economy

Let me conclude with my last topic on expanding the role of design in our lives.

Strengthening competitiveness of our design firms

Dr Teo Ho Pin and Mr Darryl David asked about the Design 2025 Masterplan. Singapore’s design firms have done well. Through the Masterplan, we want to support them further, so they can grow and thrive beyond our shores.

One initiative is Design Accreditation. Since the launch of our accreditation programme for Landscape Architects last October, 120 have been accredited. About 80 per cent of firms have at least one accredited Landscape Architect. Among them is Mr Franklin Po of Tierra Design, winner of the President’s Design Award in 2015.  Franklin is a Singaporean designer who used to work in the US, before returning to start Tierra Design.  He is now developing other young designers in Singapore, and giving them opportunities to work on different interesting projects.

We hope to have another 40 accredited Landscape Architects by the end of 2016. We also plan to expand the programme to other design sectors, starting with interior designers, as well as urban designers and planners.

Another initiative is the Singapore Good Design Mark, or SG Mark. This recognises exceptionally designed products and services that are developed in Singapore. In the last three years, SG Mark has gained considerable traction with our trade partners. We will work with the Design Business Chamber of Singapore to further establish SG mark as a regional standard for good design.

Expanding the role of design in Government

As a major consumer of design services, Government too, can play a part by providing opportunities to design firms of all sizes to take part in government projects. This way, promising local firms will get the chance to demonstrate their capabilities and build up their track records. When I met some of the local designers, they told me that such opportunities are more important and more valuable to them than government grants.

Bringing design into the community

We also want to involve our citizens and communities in the design process to co-create conducive spaces to live, work and play.

In Yuhua, for example, the DesignSingapore Council is working with the Citizens’ Consultative Committee to organise co-creation sessions for residents and community leaders. During these sessions, participants get to redesign and co-create programmes to engage their communities and enhance the experiences of residents.

This user-centric approach is key not only in design, but also in our journey towards a Smart Nation, as noted by Dr Teo Ho Pin. Design humanises technology, and makes it smarter and more user-friendly.

Infusing design into our national skillset

Mr Darryl David mentioned that design is not only for designers, I agree with him. It is something that can benefit everyone and across different sectors. To infuse design into our national skillset, DesignSingapore will work with six secondary schools to pilot enrichment workshops in design thinking. Students can pick up design thinking skills and apply these in their projects. They may or may not become designers in the future but we believe that design thinking can benefit them and can benefit different aspects of their lives. So we will consider the suggestions from Mr Darryl David to further enhance our efforts to ensure a nation of creative thinkers from young.