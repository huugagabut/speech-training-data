Imperative for digitalisation

Good morning.  Let me start by warmly welcoming all of you to IMDA’s inaugural Singapore Digital (SG:D) Industry Day.

The aim of this event is to demonstrate to industry players, like you, how you can participate in Singapore’s journey of digital transformation.  To show that businesses - of any size, in any sector, and at any stage of digitalisation - can seize opportunities and boost growth in the digital economy.  

Aligned with our Smart Nation vision, the government is working with industry to chart Singapore’s path towards digital transformation.  The advent and adoption of disruptive technologies has revolutionised the global economy, changed business models and the nature of jobs.  It has also created new possibilities for our businesses and for Singapore’s development.  At the enterprise level, going digital can help companies increase productivity, achieve greater scale, serve customers better and generate new streams of earnings.

To maximise such benefits, digitalisation must be pervasive, cutting across all sectors so as to raise the capacity and competitiveness of the entire Singapore economy.  That is why we are pursuing new and enhanced initiatives, to uplift all our businesses through digitalisation.

The Digital Economy Framework for Action charts our way forward and outlines our efforts to build Singapore’s digital competitiveness and become a global node in Asia.  IMDA will share more about the strategies and programmes laid out in the document. 

Adoption of digital technologies across sectors

While the digital economy holds much promise, going digital may be daunting for many enterprises, especially given the challenges of today’s complex and competitive economic landscape.  Some companies find it difficult to even take the first step towards digitalisation.  Others have already started and gained some momentum, but need help to go further in their digitalisation journey.  We want to help all our companies succeed in this transformation, and are thus ramping up our efforts to accelerate the adoption of digital technologies.  

SMEs Go Digital project management services

While the whole economy can gain from digitalisation, some segments may face greater challenges in making this transition.  A case in point are our SMEs.  Collectively, they employ two thirds of the workforce and contribute half of Singapore’s GDP.  Hence, digitalisation of SMEs can have a profound impact on our economy.  Yet, our SMEs may be constrained by a lack of scale, resources or expertise.

It is precisely for this reason that the SMEs Go Digital programme was conceived - to make it simple for SMEs to digitalise. Today, SMEs Go Digital has helped close to 1,000 SMEs.  This is an encouraging start but we can and must certainly do more to raise our SMEs’ digital capabilities. 

The challenge does not lie merely in the adoption of digital technologies.  Rather, the greater challenge lies in sustaining the momentum and adapting to the fast changing environment.  In other words, SMEs need to be able to effectively implement digital solutions to reap the full rewards of digitalisation.  This means reviewing business processes, redesigning job roles and managing the transition to a digital paradigm.  However, many SMEs do not have the requisite expertise to do this.  

Therefore, we are enhancing the SMEs Go Digital efforts to make such expertise available to SMEs.  IMDA is working with other government agencies, NTUC, and the Singapore Manufacturing Federation (SMF), the first pilot operator, to offer digital project management services for SMEs.  We will train, certify and deploy experienced PMEs as in-house digital project managers.  SMEs embarking on digital projects can also receive funding support for such services. 

As a trial, the Singapore Manufacturing Federation (SMF) has provided digital project management services to businesses in Kampong Glam.  IMDA’s Kampong Glam digitalisation project is under way, and a key initiative is to drive merchants’ adoption of integrated point-of-sale (POS) systems.  The digital project managers have guided two F&B businesses, KokonoE (‘ko-ko-nay’) and Gloria Jean’s Coffees, to implement POS systems, helping them modify their processes to maximise business outcomes.  We look forward to many more companies emulating their success.  To this end, IMDA will continue to explore other partnerships with industry to support the digital transformation of our SMEs.  

E-invoicing and the adoption of PEPPOL standard

We also need shifts at the system level to be better prepared for the digital economy.  E-invoicing is one example.  Invoicing is a common and essential aspect of business transactions in the economy.  E-invoicing can be adopted by all businesses as they begin their digitalisation journey, and serve as a shared reference point for the whole economy. 

Traditional invoicing is manual, prone to human error, and can be costly for businesses.  For instance, logistics firm GOGOVAN handles hundreds of corporate deliveries daily, with separate invoices for each client which can take up to eight days to process.  GOGOVAN, and similar companies, can benefit from e-invoicing, which would help to cut costs, accelerate invoice processing and shorten payment times.  Moreover, companies would also have real-time visibility of payment cycles, allowing them to better manage cash flows. 

Despite these obvious benefits of e-invoicing, it has been challenging to drive its mass adoption to achieve network effects.  This is because e-invoicing is currently fragmented, with many solutions that are proprietary and not interoperable.  That is why we have been studying the development of an e-invoicing framework.  

Today, I am happy to announce that we will be implementing the Pan-European Public Procurement On-Line (PEPPOL) as the nationwide e-invoicing standard in Singapore.  Through this interoperable and low-cost standard, Singapore companies can more easily adopt e-invoicing on a large scale.  In addition, this will help our companies transact internationally, with businesses from many other countries that are also in the PEPPOL network.

Our Government will lead the way for adoption, with agencies connecting government systems and processes, such as Vendors@Gov, to the framework.  The National Trade Platform is exploring how to make PEPPOL one of the key international standards for trade documents accepted on the platform.  Other government agencies supportive of the framework include the Accountant-General’s Department, GovTech and the Maritime and Port Authority of Singapore.

From industry, many large players and associations have recognised the benefits of e-invoicing, and are looking to integrate their organisations, partners and members into the framework.  For example, in the retail foods sector, lead buyers like NTUC FairPrice and Dairy Farm are keen to adopt the standard and help bring their suppliers on board.  There are already over 40 companies who are keen to participate. 

Together with the Singapore Business Federation (SBF) Digitalisation Committee and Enterprise Singapore, we will promote e-invoicing across the wider business community.  Ultimately, the benefit of e-invoicing lies in scale of the network, and the breadth and depth of its adoption.  Hence, it is important that all companies participate in and benefit from e-invoicing. 

Building digital capabilities of the media sector

Beyond such system level initiatives, and our work with SMEs, we also need sector-specific responses to digital transformation.  Whole sectors are being disrupted by technology, redefining business models and jobs.  Our response must be to help our sectors move up the digitalisation ladder by building deep capabilities.  Over the past few months, we have shared our plans to transform the ICM sector.  Today, let me focus on how we aim to develop a competitive media sector through digitalisation.

New technologies, changing consumption patterns, and new entrants (such as Over-The-Top streaming platforms and integrated ecosystem players) are disrupting the media industry globally.  Local media companies must acquire new capabilities to stay competitive in this evolving landscape.

To support the media sector in this endeavour, we are introducing an enhanced suite of programmes under our Future of Media strategy.  One key programme is to deepen the content creation capabilities of our media companies.  We will work with partners to help media companies harness technologies such as data analytics or AR/VR, to produce engaging content for an increasingly digital audience.

In addition, we will help our companies leverage the significant potential of digital streaming platforms.  For instance, home-grown and Emmy-nominated series Oddbods, produced by One Animation, has been picked up for distribution by Netflix and Amazon Prime Video.  Additionally, Oddbods’ videos have accumulated 3.5 billion views on YouTube and other digital platforms.  We want their success to inspire more companies to explore new streaming platforms to enhance outreach.

I would like to urge media companies to work closely with us, to nurture an innovative and competitive media sector that creates quality content and harnesses new technologies.

Development of innovative digital products and business models

We must also recognise that this age of disruption offers possibilities for experimentation with new digital products, services and business models.  Fortune will favour those who embrace the change and are nimble and creative in their response. 

Hence, we must set our sights higher.  Beyond adopting technologies and building capabilities, we want to support our companies in taking the next big step in innovation and experimentation, to tackle complex business challenges and seize new growth opportunities. 

One initiative to drive such efforts is the Open Innovation Platform (OIP), which was announced at Budget this year.  The OIP will bring together tech companies and user companies to develop innovative solutions for real business problems.  This will give local ICM companies opportunities to collaborate with diverse industry players, and create digital solutions that can be scaled and exported.  IMDA will share more on the progress of the OIP.

Catalysing digital platform businesses

The digital revolution has also given rise to new technology-enabled, platform-driven business models, which have had a profound transformative impact on the digital economy.  These digital platforms create value by aggregating ecosystems of end-users and producers, and using technology to facilitate transactions between them.  Such platforms enable information sharing, collaboration and promote the creation of new products and services.  

We have seen that digital platforms can enhance the delivery of AI and data services to different kinds of users.  These range from companies that monetise data, to developers seeking data sets to build applications, and end-user companies using apps to analyse data and derive insights.  Hence, the growth of such platforms can generate value for many enterprises in the ecosystem. 

But such platform businesses are not easy to build, and require time to cultivate its ecosystem of users before it can enjoy the benefits of network effects.  Hence, to kickstart the development of platform networks in Singapore, IMDA will support companies to build innovative and commercially viable digital platforms that offer data and AI services to other players. 

We have identified four companies to work with.  One of these is DataStreamX, which operates a data transaction and marketplace platform.  Through the digital platforms programme, DataStreamX will be enabling an AI and micro-services layer on top of its existing commercial platform.  This will allow individuals or companies to build services on top of the data sources from the DataStreamX marketplace.  Examples could include chatbots to solve business and consumer needs.

These are early days and these initiatives are just the beginning.  Our businesses can expect more programmes that will help them generate value through innovation. 

Conclusion

To conclude, I wish to emphasise that the goal of a productive, innovative and sustainable digital economy is well within our reach.  The government looks forward to collaborating with industry to increase adoption, deepen capabilities and drive innovation.  This will also put Singapore in good stead to enhance our economic competitiveness, and serve as an important global node for technology, innovation and enterprise.

As an economy and as a nation, we must continually reinvent ourselves to keep pace with the changes wrought by the digital economy, while capitalising on the multitude of opportunities and possibilities that it presents.  We look forward to working closely with our businesses and all Singaporeans to succeed in this important mission.